import { FC } from "react";
import { Col, Grid, Panel, Progress, Radio, Row } from "rsuite";
import { WhatEvent } from 'models/event';

export interface EventCardPanelProps {
    header: string;
    data: WhatEvent;

}


export const EventCardPanel: FC<EventCardPanelProps> = (props: EventCardPanelProps) => {

    const { header, data } = props

    return (
        <Panel shaded bordered bodyFill style={{ display: 'inline-block', width: 230, margin: 10 }} onClick={() => { console.log(data.id, data.title, data); window.location.href="create-event?"+data.id }}>
            {/* <img src="https://via.placeholder.com/140x180" height="140" width={180} /> */}
            <Panel header={header}>
                <p>Ticket issued 70/100</p>
            </Panel>
            <Progress.Line percent={70} status="active" />
            <Grid fluid>
                <Row className="show-grid">
                    <Col xs={12}>
                        <Radio checked={false}>Free</Radio>
                    </Col>
                    <Col xs={12}>
                        <Radio checked> Paid</Radio>
                    </Col>
                </Row>
            </Grid>
        </Panel>
    )
}