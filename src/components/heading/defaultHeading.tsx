import { FC } from "react";
import './style.scss'


export interface DefaultHeadingProps {
    h?: string,
    s?: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10';
    b?: boolean;
    i?: boolean;
    c?: 'green' | 'yellow' | 'red' | 'blue' | 'black' | 'white';

}



const DefaultHeading = (props: DefaultHeadingProps) => {

    const {
        h = 'Heading',
        s = '5',
        b = true,
        i = false,
        c,
    } = props

    return (
        <span className={`default-heading ${c} ${b ? 'b' : ''} ${i ? 'i' : ''} ${s ? 's-' + s : ''}`}>
            {h}
        </span>
    )
}

export default DefaultHeading