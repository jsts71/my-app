import { FC } from "react"
import { Pagination, PaginationProps } from "rsuite"


export interface DefaultPaginationProps extends PaginationProps {

}

export const DefaultPagination: FC<DefaultPaginationProps> = (props: DefaultPaginationProps) => {

    return (
        <Pagination prev last next first size="lg" limit={parseInt(process.env.REACT_APP_PAGINATION_LIMIT ?? '15')} {...props} />
    )
}