import { FC } from "react";
import { Message, MessageProps } from "rsuite";

export interface DefaultMessageProps {
    message: string,
    type: 'info' | 'success' | 'warning' | 'error'
}

export const DefaultMessage:FC<DefaultMessageProps> = (props: DefaultMessageProps) => {
    const { message, type } = props
    return (
        <Message showIcon type={type} duration={500}>
            {message}
        </Message>
    )
};
