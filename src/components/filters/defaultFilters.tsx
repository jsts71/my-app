import { FC } from "react";
import { DateRangePicker, FlexboxGrid, Input, InputGroup, SelectPicker } from "rsuite";
import SearchIcon from '@rsuite/icons/Search';

const styles = {
    marginBottom: 10
};
const data = [{ label: 'Id', value: 'id' }, { label: 'Title', value: 'title' }];

export interface DefaultFiltersProps {

}

export const DefaultFilters: FC<DefaultFiltersProps> = (props: DefaultFiltersProps) => {

    return (

        <div className="show-grid">
            <FlexboxGrid>
                <FlexboxGrid.Item colspan={8} order={1}>
                    <InputGroup {...props} inside style={styles}>
                        <Input placeholder='Search by name' />
                        <InputGroup.Button>
                            <SearchIcon />
                        </InputGroup.Button>
                    </InputGroup>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={8} order={2}>
                    <SelectPicker data={data} block placeholder="Sort by"/>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={8} order={3}>
                    <DateRangePicker block placeholder="Select date range"/>
                </FlexboxGrid.Item>
            </FlexboxGrid>
        </div>
    )
}