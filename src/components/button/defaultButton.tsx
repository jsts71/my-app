import { Button, ButtonProps, IconButton, IconButtonProps } from "rsuite"
import './style.scss'

export interface DefaultButtonProps extends IconButtonProps, ButtonProps {
    text?: string;
    onClick: () => void;
    bg_color?: string;
}

const DefaultButton = (props: DefaultButtonProps) => {

    const {
        icon,
        text,
        onClick,
        bg_color,
    } = props

    return (
        <>
            {!icon ?
                <Button className="default-button" {...props} style={{ backgroundColor: bg_color }}>{text}</Button> :
                <IconButton className="default-button" {...props} style={{ backgroundColor: bg_color }}>{text}</IconButton>}
        </>

    )
}

export default DefaultButton