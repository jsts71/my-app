import React, { FC, Dispatch } from 'react';
import { Editor, EditorState, RichUtils } from 'draft-js';
import { BlockStyleControls, InlineStyleControls } from './draftjs-required-components';
import './style.scss';

export interface DefaultDraftViewProps {
    editorState: EditorState;
    color?: 'black' | 'green' | 'white' | 'red' | string;
    border?: boolean;
}

export const DefaultDraftView: FC<DefaultDraftViewProps> = (props: DefaultDraftViewProps) => {

    const { editorState, color, border = true } = props


    return (
        <>
            <div className="draft-view" style={{ color: color, border: border ? '1px solid black' : 'none' }}>
                <Editor editorState={editorState} onChange={() => { }} />
            </div>
        </>
    )
}