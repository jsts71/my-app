import React, { FC, Dispatch } from 'react';
import { Editor, EditorState, RichUtils } from 'draft-js';
import { BlockStyleControls, InlineStyleControls } from './draftjs-required-components';
import './style.scss';

export interface DefaultDraftEditorProps {
    editorState: EditorState;
    setEditorState: (value: EditorState) => void
}

export const DefaultDraftEditor: FC<DefaultDraftEditorProps> = (props: DefaultDraftEditorProps) => {

    const { editorState, setEditorState } = props


    const toggleBlockType = (blockType: string) => {
        setEditorState(RichUtils.toggleBlockType(editorState, blockType));
    };

    const toggleInlineStyle = (inlineStyle: string) => {
        setEditorState(RichUtils.toggleInlineStyle(editorState, inlineStyle));
    };

    return (
        <>

            <BlockStyleControls
                editorState={editorState}
                onToggle={toggleBlockType}
            />

            <InlineStyleControls
                editorState={editorState}
                onToggle={toggleInlineStyle}
            />

            <div className="draft-editor">
                <Editor editorState={editorState} onChange={setEditorState} />
            </div>
        </>
    )
}