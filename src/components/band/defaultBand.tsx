import { FC } from "react";
import "./style.scss"

export interface DefaultBandProps {
    text: string;
    subtext?: string;
}

export const DefaultBand: FC<DefaultBandProps> = (props: DefaultBandProps) => {

    const { text, subtext } = props

    return (
        <div className="default-band">
            {text}
            <p>
                {subtext}
            </p>
        </div>
    )
}