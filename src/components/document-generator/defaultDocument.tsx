import { PDFViewer, Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';


// Create styles
const styles = StyleSheet.create({
    page: { padding: 60 },
    box: { width: '100%', marginBottom: 30, borderRadius: 5 },
    pageNumbers: {
        position: 'absolute',
        bottom: 20,
        left: 0,
        right: 0,
        textAlign: 'center'
    },
});


export interface DefaultDocumentProps {

}

export const DefaultDocument = () => {

    return (
        <>
            <PDFViewer width="100%" height="1075px">
                <Document >
                    <Page style={styles.page} size="A4" wrap>
                        <View style={[styles.box, { height: 400, backgroundColor: 'tomato' }]} />
                        <View style={[styles.box, { height: 280, backgroundColor: 'crimson' }]} />
                        <View style={[styles.box, { height: 600, backgroundColor: 'deepskyblue' }]} />
                        <View style={[styles.box, { height: 400, backgroundColor: 'tomato' }]} />
                        <Text style={styles.pageNumbers} render={({ pageNumber, totalPages }) => (
                            `${pageNumber} / ${totalPages}`
                        )} fixed />
                    </Page>
                </Document>
            </PDFViewer>
        </>
    )
}