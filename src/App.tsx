import { RouterProvider } from 'react-router-dom';
import withDefaultLayout from 'layouts/withDefault.layout.hoc';

import './App.css';

import { router } from 'utils/router';

function App() {

  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default withDefaultLayout(App);
