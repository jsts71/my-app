import { FC } from "react";
import { withSuperContext } from "hoc/withSuperContext";
import logo from 'assets/auth/site-logo-3.png'

import "./style.scss"
import { Carousel, Col, Row } from "rsuite";

export interface AuthLayoutProps {

}

const withAuthLayout = (WrappedComponent: FC) => {

    function NewFunctionComponent(props: AuthLayoutProps) {


        // const superContext = useContext(SuperContext);

        return (
            <Row className="show-grid auth-layout">
                <Col className="left-col" md={24} lg={12}>
                    <Carousel autoplay className="custom-slider">
                        <img id="img-1" src="https://images.unsplash.com/photo-1566808907623-51b8fc382454?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" ></img>
                        <img id="img-2" src="https://images.unsplash.com/photo-1604515438635-fd331c877a6b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80" />
                        <img id="img-3" src="https://images.unsplash.com/photo-1542813885-38d1b9698bca?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" />
                        <img id="img-4" src="https://images.unsplash.com/photo-1583795312373-ee989421e4e2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" />
                        <img id="img-5" src="https://images.unsplash.com/photo-1590026772735-9997c27e2083?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80" />
                    </Carousel>
                </Col>
                <Col className="right-col" md={24} lg={12}>
                    <img id="band-logo" src={logo} /><br/>
                    <div className="wrapped-comp">
                        <WrappedComponent />
                    </div>
                </Col>
            </Row>
        )
    }
    return withSuperContext(NewFunctionComponent)
}

export default withAuthLayout