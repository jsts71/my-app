import { FC, useContext } from "react";
import { Container, Content, Footer, Header, Sidebar } from "rsuite";
import { ToastContainer } from 'react-toastify';
import { ConstSideBar } from "features/sidebars/const.side-bar";
import { ConstHeader } from "features/headers/const.headers";
import { ConstFooter } from "features/footers/const.footer";
import { withSuperContext } from "hoc/withSuperContext";
import { SuperContext } from 'utils/contexts';

import "./style.scss"

export interface DefaultLayoutProps {

}

const withDefaultLayout = (WrappedComponent: FC) => {

    function NewFunctionComponent(props: DefaultLayoutProps) {

        const superContext = useContext(SuperContext);

        return (
            <div className="default-layout">
                <Container>
                    <Sidebar
                        style={{ display: 'flex', flexDirection: 'column' }}
                        width={superContext.screenWidth > 720 ? 300 : 56}
                        collapsible
                    >
                        <ConstSideBar />
                    </Sidebar>
                    <Container>
                        <Header>
                            <ConstHeader />
                        </Header>
                        <Content>
                            <WrappedComponent />
                        </Content>
                        <Footer>
                            <ConstFooter />
                        </Footer>
                    </Container>
                </Container>

                <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light" />
            </div>
        )
    }
    return withSuperContext(NewFunctionComponent)
}

export default withDefaultLayout