import { createRef, FC, useContext, useEffect, useRef, useState } from "react"
import { Button, IconButton, Nav, PickerHandle, SelectPicker, Sidenav } from "rsuite"

import SearchIcon from '@rsuite/icons/Search';
import CloseIcon from '@rsuite/icons/Close';
import AppSelectIcon from '@rsuite/icons/AppSelect';
import TagDateIcon from '@rsuite/icons/TagDate';
import CouponIcon from '@rsuite/icons/Coupon';
import SpeakerIcon from '@rsuite/icons/Speaker';
import DeviceOtherIcon from '@rsuite/icons/DeviceOther';
import ScatterIcon from '@rsuite/icons/Scatter';
import TmallIcon from '@rsuite/icons/Tmall';
import PeoplesCostomizeIcon from '@rsuite/icons/PeoplesCostomize';

import DashboardIcon from '@rsuite/icons/legacy/Dashboard';
import MagicIcon from '@rsuite/icons/legacy/Magic';

import "./style.scss"
import { SuperContext } from "utils/contexts";
import { useLocation } from "react-router-dom";

export interface ConstSideBarProps {
}

export const ConstSideBar: FC<ConstSideBarProps> = (props: ConstSideBarProps) => {

    const data = ['Current Event', 'Details', 'Where & When', 'Ticket Types', 'Payment', 'Introduction', 'Terms & Condition', 'Staff / Crew Tickets', 'Discount Codes'].map(
        item => ({ label: item, value: item })
    );

    const [showPicker, setShowPicker] = useState<boolean>(false)
    const superContext = useContext(SuperContext);

    window.addEventListener('click', function (e: any) {
        if (document.getElementById('clickbox')?.contains(e.target)) { }
        else setShowPicker(false)
    });

    const [path, setPath] = useState<string>(window.location.pathname)
    const [open, setOpen] = useState<string>('')

    useEffect(() => {
        setPath(window.location.pathname)
        if (path == "/event-details"
            || path == "/event-category"
            || path == "/event-where-n-when") setOpen('2')
        else if (path == "/ticket"
            || path == "/ticket-types"
            || path == "/ticketing-payment"
            || path == "/ticketing-introduction"
            || path == "/terms-n-condition"
            || path == "/ticketing-staff-crew"
            || path == "/discount-code") setOpen('3')
        else if (path == "/volunteer-application"
            || path == "/volunteer-register"
            || path == "/volunteer-ticket"
            || path == "/volunteer-finance"
            || path == "/volunteer-email"
            || path == "/volunteer-export"
            || path == "/volunteer-position"
            || path == "/volunteer-bulk-delete"
            || path == "/volunteer-shift-time") setOpen('5')
        else if (path == "/application-builder"
            || path == "/volunteers-application"
            || path == "/market-application"
            || path == "/application-performance"
            || path == "/application-email") setOpen('6')
        else if (path == "/template-list"
            || path == "/template-editor") setOpen('8')
    }, [])

    return (
        <>
            <Sidenav className="const-sidenav" expanded={superContext.screenWidth > 720}>
                <Sidenav.Header id='clickbox' >
                    {superContext.screenWidth > 720 ?
                        <SelectPicker data={data} placeholder="Select Event" block style={{ height: '35px', margin: '10px' }} /> :
                        <>
                            <Nav style={{ height: '35px', margin: '5px auto' }}>
                                <Nav.Item icon={<SearchIcon />} onClick={() => setShowPicker(true)}>Select Event</Nav.Item>
                            </Nav>
                            <div style={{ top: '8px', left: '10px', width: '300px', position: 'absolute', display: `${showPicker ? 'block' : 'none'}` }}>
                                <SelectPicker size="lg" data={data} placeholder="Select Event" style={{ width: '250px' }} />
                                <IconButton icon={<CloseIcon />} appearance='subtle' onClick={() => setShowPicker(false)} />
                            </div>
                        </>
                    }
                </Sidenav.Header>
                <hr />

                <Sidenav.Body>
                    <Nav>
                        <Nav.Item eventKey="1" icon={<AppSelectIcon />} href="/current-event" active={path == "/current-event"}>
                            Current Event
                        </Nav.Item>

                        <Nav.Menu eventKey="2" title="Event Settings" icon={<TagDateIcon />} placement="rightStart" open={open == '2'} onClick={() => setOpen('2')} trigger="hover">
                            <Nav.Item eventKey="2-1" href="/event-details" active={path == "/event-details"}>Details</Nav.Item>
                            <Nav.Item eventKey="2-2" href="/event-category" active={path == "/event-category"}>Event Category</Nav.Item>
                            <Nav.Item eventKey="2-2" href="/event-where-n-when" active={path == "/event-where-n-when"}>Where & When</Nav.Item>
                        </Nav.Menu>

                        <Nav.Menu eventKey="3" title="Ticketing" icon={<CouponIcon />} placement="rightStart" open={open == '3'} onClick={() => setOpen('3')} trigger="hover">
                            <Nav.Item eventKey="3-1" href="/ticket" active={path == "/ticket"}>Ticket</Nav.Item>
                            <Nav.Item eventKey="3-2" href="/ticket-types" active={path == "/ticket-types"}>Ticket Types</Nav.Item>
                            <Nav.Item eventKey="3-3" href="/ticketing-payment" active={path == "/ticketing-payment"}>Payment</Nav.Item>
                            <Nav.Item eventKey="3-4" href="/ticketing-introduction" active={path == "/ticketing-introduction"}>Introduction</Nav.Item>
                            <Nav.Item eventKey="3-5" href="/terms-n-condition" active={path == "/terms-n-condition"}>Terms & Condition</Nav.Item>
                            <Nav.Item eventKey="3-6" href="/ticketing-staff-crew" active={path == "/ticketing-staff-crew"}>Staff / Crew Tickets</Nav.Item>
                            <Nav.Item eventKey="3-7" href="/discount-code" active={path == "/discount-code"}>Discount Codes</Nav.Item>
                        </Nav.Menu>

                        <Nav.Item eventKey="4" icon={<DashboardIcon />} href="/sales-report" active={path == ""}>
                            Sales & Report
                        </Nav.Item>

                        <Nav.Menu eventKey="5" title="Volunteers" icon={<SpeakerIcon />} placement="rightStart" open={open == '5'} onClick={() => setOpen('5')} trigger="hover">
                            <Nav.Item eventKey="5-1" href="/volunteer-application" active={path == "/volunteer-application"}>Volunteer Applications</Nav.Item>
                            <Nav.Item eventKey="5-2" href="/volunteer-register" active={path == "/volunteer-register"}>Register Volunteers</Nav.Item>
                            <Nav.Item eventKey="5-3" href="/volunteer-ticket" active={path == "/volunteer-ticket"}>Volunteer Ticket</Nav.Item>
                            <Nav.Item eventKey="5-4" href="/volunteer-finance" active={path == "/volunteer-finance"}>Finance</Nav.Item>
                            <Nav.Item eventKey="5-5" href="/volunteer-email" active={path == "/volunteer-export"}>Email</Nav.Item>
                            <Nav.Item eventKey="5-6" href="/volunteer-export" active={path == "/volunteer-export"}>Export</Nav.Item>
                            <Nav.Item eventKey="5-7" href="/volunteer-position" active={path == "/volunteer-position"}>Volunteer Positions</Nav.Item>
                            <Nav.Item eventKey="5-8" href="/volunteer-shift-time" active={path == "/volunteer-shift-time"}>Shift Times</Nav.Item>
                            <Nav.Item eventKey="5-9" href="/volunteer-bulk-delete" active={path == "/volunteer-bulk-delete"}>Bulk Delete</Nav.Item>
                        </Nav.Menu>

                        <Nav.Menu eventKey="6" title="Applications" icon={<DeviceOtherIcon />} placement="rightStart" open={open == '6'} onClick={() => setOpen('6')} trigger="hover">
                            <Nav.Item eventKey="6-1" href="/application-builder" active={path == "/application-builder"}>Application Builder</Nav.Item>
                            <Nav.Item eventKey="6-2" href="/volunteers-application" active={path == "/volunteers-application"}>Volunteers Application</Nav.Item>
                            <Nav.Item eventKey="6-3" href="/market-application" active={path == "/market-application"}>Markets Application</Nav.Item>
                            <Nav.Item eventKey="6-4" href="/application-performance" active={path == "/application-performance"}>Performers Application</Nav.Item>
                            <Nav.Item eventKey="6-5" href="/application-email" active={path == "/application-email"}>Application Emails</Nav.Item>
                        </Nav.Menu>

                        <Nav.Item eventKey="7" icon={<ScatterIcon />} href="/staff" active={path == "/staff"}>
                            Staff
                        </Nav.Item>

                        <Nav.Menu eventKey="8" title="Email Templates" icon={<TmallIcon />} placement="rightStart" open={open == '8'} onClick={() => setOpen('8')} trigger="hover">
                            <Nav.Item eventKey="8-1" href="/template-list" active={path == "/template-list"}>Templates (List of temples)</Nav.Item>
                            <Nav.Item eventKey="8-2" href="/template-editor" active={path == "/template-editor"}>Editor (A screen with the following)</Nav.Item>
                        </Nav.Menu>

                        <Nav.Item eventKey="9" icon={<PeoplesCostomizeIcon />} href="/user-portal" active={path == "/user-portal"}>
                            Portal Users
                        </Nav.Item>
                    </Nav>
                </Sidenav.Body>
            </Sidenav>
        </>
    )
}