import { FC } from "react";
import { List, FlexboxGrid, Loader, ButtonToolbar, IconButton } from 'rsuite';
import { Ticket } from "models/ticket";
import EditIcon from '@rsuite/icons/Edit';
import TrashIcon from '@rsuite/icons/Trash';
import EyeCloseIcon from '@rsuite/icons/EyeClose';
import { DefaultPagination } from "components/pagination/defaultPagination";
import { useTicketListHook } from "./hooks/useTicketList.hook";
import "./style.scss"

export interface TicketListProps {
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
    setId: (value: number) => void
}

const styleCenter = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '60px'
};

export const TicketList: FC<TicketListProps> = (props: TicketListProps) => {

    const { setView, setId } = props
    const { onClickRemove, isLoading, isSuccess, activePage, setActivePage, data } = useTicketListHook({})

    return (
        <>
            {isLoading && <Loader center content="loading" />}
            {isSuccess &&
                <>
                    <List>
                        <List.Item key={0} index={0}>
                            <FlexboxGrid style={{ color: "green" }}>
                                <FlexboxGrid.Item colspan={2} style={styleCenter}>ID</FlexboxGrid.Item>
                                <FlexboxGrid.Item colspan={6} style={{ ...styleCenter, justifyContent: 'left' }}>NAME</FlexboxGrid.Item>
                                <FlexboxGrid.Item colspan={6} style={styleCenter}>TYPE</FlexboxGrid.Item>
                                <FlexboxGrid.Item colspan={4} style={styleCenter}>DISCOUNT</FlexboxGrid.Item>
                                <FlexboxGrid.Item colspan={6} style={styleCenter}>ACTION</FlexboxGrid.Item>
                            </FlexboxGrid>
                        </List.Item>
                    </List>
                    <List hover>
                        {data?.results?.map((item: Ticket, index: number) => (
                            <List.Item key={item['id']} index={index + 1}>
                                <FlexboxGrid>
                                    <FlexboxGrid.Item colspan={2} style={styleCenter}>
                                        {item['id']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={6} style={{ ...styleCenter, justifyContent: 'left' }}>
                                        {item['name']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={6} style={styleCenter}>
                                        {item.is_volunteer_type ? 'Volunteer' : 'Client'}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={4} style={styleCenter}>
                                        {item['discount_percent']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={6} style={styleCenter}>
                                        <ButtonToolbar>
                                            <IconButton circle icon={<EditIcon />} color="green" appearance="subtle" onClick={() => { setView('update'); setId(item?.id ?? 0) }} />
                                            <IconButton circle icon={<EyeCloseIcon />} color="blue" appearance="subtle" onClick={() => { setView('view'); setId(item?.id ?? 0) }} />
                                            <IconButton circle icon={<TrashIcon />} color="red" appearance="subtle" onClick={() => { onClickRemove(item?.id ?? 0) }} />
                                        </ButtonToolbar>
                                    </FlexboxGrid.Item>
                                </FlexboxGrid>
                            </List.Item>
                        )) ?? ''}
                    </List>
                </>
            }
            <DefaultPagination
                total={data?.count ?? 0}
                activePage={activePage}
                onChangePage={setActivePage}
            />
        </>
    )
}