import rest_api from 'app/rest-api';
import { Ticket } from 'models/ticket';

const ticketApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createTicket: build.mutation({
            query: (payload: Ticket) => ({ method: "POST", url: '/ticket/tickets/', data: payload })
        }),
        updateTicket: build.mutation({
            query: (payload: Ticket) => ({ method: "PUT", url: '/ticket/tickets/'+payload.id+'/', data: payload })
        }),
        getAllTicket: build.mutation({
            query: (query: string) => ({ method: "GET", url: '/ticket/tickets/?' + query })
        }),
        getTicketById: build.mutation({
            query: (id) => ({ method: "GET", url: '/ticket/tickets/' + id + '/' })
        }),
        removeTicketById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/ticket/tickets/' + id })
        }),
        getTicketByCategory: build.query({
            query: (category) => ({ method: "GET", url: '/ticket/tickets', params: { category: category } })
        }),


    })
})


export const {
    useCreateTicketMutation,
    useUpdateTicketMutation,
    useGetAllTicketMutation,
    useGetTicketByIdMutation,
    useRemoveTicketByIdMutation,
    useGetTicketByCategoryQuery

} = ticketApi
export default ticketApi