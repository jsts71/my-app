import { Ticket, ticketInitialValues, ticketValidationSchema } from 'models/ticket';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { ObjectSchema } from 'yup';
import { useCreateTicketMutation, useGetTicketByIdMutation, useUpdateTicketMutation } from '../redux/apis';
import { useEffect } from 'react';

export interface TicketFormHookProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export interface TicketFormHookInterface {
    validationSchema: ObjectSchema<Ticket>
    initialValues: Ticket
    onSubmit: (values: Ticket) => void
    isLoading: boolean
    isSuccess: boolean
    isError: boolean
}

export const useTicketFormHook = (props: TicketFormHookProps): TicketFormHookInterface => {

    const { id, setView } = props
    const [initialValues, setInitialValues] = useState(ticketInitialValues)
    const [validationSchema, setValidationSchema] = useState(ticketValidationSchema)
    const [createTicket, { data, isLoading, isSuccess, isError }] = useCreateTicketMutation()
    const [getTicketsById, { data: getByIdData, isLoading: getByIdIsLoading, isSuccess: getByIdIsSuccess }] = useGetTicketByIdMutation()
    const [updateTickets, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateTicketMutation()

    useEffect(() => {
        id && getTicketsById(id)
    }, [id])

    useEffect(() => {
        id && setInitialValues(getByIdData)
    }, [getByIdIsSuccess])

    useEffect(() => {
        isSuccess && setView('list')
        isError && toast.error("Something went wrong! Failed to create new event type!")
        isSuccess && toast.success("Event type created successfully.")
    }, [isLoading, isSuccess, isError])

    useEffect(() => {
        updateIsSuccess && setView('view')
        updateIsError && toast.error("Something went wrong! Failed to update this event type!")
        updateIsSuccess && toast.success("Event type updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])

    const onSubmit = (values: Ticket) => {
        id ?
            updateTickets(values)
            :
            createTicket(values)
    }

    return {
        validationSchema,
        initialValues,
        onSubmit,
        isLoading,
        isSuccess,
        isError
    }
}