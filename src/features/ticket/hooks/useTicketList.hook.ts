import { TicketFilter } from 'models/ticket';
import { useEffect, useState } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { useGetAllTicketMutation, useRemoveTicketByIdMutation } from '../redux/apis';

export interface TicketFormHookProps {

}

export interface TicketFormHookInterface {
    onClickRemove: (id: number) => void
    isLoading: boolean
    isSuccess: boolean
    activePage: number
    setActivePage: (value: number) => void
    data: any
}

export const useTicketListHook = (props: TicketFormHookProps): TicketFormHookInterface => {

    const [activePage, setActivePage] = useState(1);
    const [getAllTicket, { data, isLoading, isSuccess }] = useGetAllTicketMutation()
    const [removeTicketById, { data: removedData, isLoading: removedIsLoading, isSuccess: removedIsSuccess, isError: removeIsError }] = useRemoveTicketByIdMutation()
    var filter = new TicketFilter();

    const onClickRemove = (id: number) => {
        confirmAlert({
            title: 'Remove ticket type ' + id,
            message: 'Are you sure to remove this ticket type?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => removeTicketById(id)
                },
                {
                    label: 'No',
                    onClick: () => toast.info('You cancel to delete!')
                }
            ]
        });
    };

    useEffect(() => {
        filter.setPage(activePage)
        getAllTicket(filter.to_string())

    }, [activePage])

    useEffect(() => {
        filter.setPage(activePage)
        removedIsSuccess && toast.success('Ticket type removed successfully!')
        !removedIsSuccess ?? getAllTicket(filter.to_string()); setActivePage(1); filter.setPage(activePage); getAllTicket(filter.to_string())
        removeIsError && toast.error('Ticket type failed to remove!')
    }, [removedIsSuccess, removedIsLoading, removeIsError])

    return {
        onClickRemove,
        isLoading,
        isSuccess,
        activePage,
        setActivePage,
        data
    }
}