
import { UserRegistration } from 'models/user';
import { object, string, number, date, InferType, ObjectSchema, ref } from 'yup';


export interface RegistrationFormHookProps {

}

export interface RegistrationFormHookInterface {
    registrationSchema: ObjectSchema<UserRegistration>
    initialValues: UserRegistration
    onSubmit: (values: UserRegistration) => void

}

export const useRegistrationFormHook = (props: RegistrationFormHookProps): RegistrationFormHookInterface => {

    let registrationSchema = object({
        first_name: string().required('First name is a required field.'),
        last_name: string().required('Last name is a required field.'),
        username: string().required('Username is a required field.'),
        email: string().email('Enter a valid email').required('Email is a required field.'),
        password: string().required('Password is a required field.'),
        confirm_password: string().required('Confirm password is a required field.').oneOf([ref('password')], 'Passwords must match.')
    });

    let initialValues = {
        first_name: '',
        last_name: '',
        username: '',
        email: '',
        password: '',
        confirm_password: '',
    }

    const onSubmit = (values: UserRegistration) => {
        console.log(values)
    }

    return {
        registrationSchema,
        initialValues,
        onSubmit
    }
}