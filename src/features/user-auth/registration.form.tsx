import { FC } from "react";
import { Button, ButtonGroup, ButtonToolbar, Col, Form, Input, Row } from "rsuite";
import { useRegistrationFormHook } from "./hooks/useRegistrationForm.hook";
import { useFormik } from 'formik';

import "./style.scss"

export interface RegistrationFormProps {

}

export const RegistrationForm: FC<RegistrationFormProps> = (props: RegistrationFormProps) => {

    const { registrationSchema, initialValues, onSubmit } = useRegistrationFormHook({})
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: registrationSchema });
    const { values, setFieldValue, handleSubmit, errors } = formik
    console.log(errors)
    return (
        <Form fluid>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="first_name">
                        <Form.ControlLabel>First Name</Form.ControlLabel>
                        <Form.Control name="first_name" onChange={(v: string) => setFieldValue("first_name", v)} value={values.first_name} />
                        <Form.HelpText>{errors.first_name && errors.first_name}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="last_name">
                        <Form.ControlLabel>Last Name</Form.ControlLabel>
                        <Form.Control name="last_name" onChange={(v: string) => setFieldValue("last_name", v)} />
                        <Form.HelpText>{errors.first_name && errors.first_name}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group controlId="username">
                <Form.ControlLabel>User Name</Form.ControlLabel>
                <Form.Control name="user_name" onChange={(v: string) => setFieldValue("username", v)} />
                <Form.HelpText>{errors.username && errors.username}</Form.HelpText>
            </Form.Group>
            <Form.Group controlId="email">
                <Form.ControlLabel>Email</Form.ControlLabel>
                <Form.Control name="email" type="email" onChange={(v: string) => setFieldValue("email", v)} />
                <Form.HelpText>{errors.email && errors.email}</Form.HelpText>
            </Form.Group>
            <Form.Group controlId="password">
                <Form.ControlLabel>Password</Form.ControlLabel>
                <Form.Control name="password" type="password" autoComplete="off" onChange={(v: string) => setFieldValue("password", v)} />
                <Form.HelpText>{errors.password && errors.password}</Form.HelpText>
            </Form.Group>
            <Form.Group controlId="confirm_password">
                <Form.ControlLabel>Confirm Password</Form.ControlLabel>
                <Form.Control name="confirm_password" type="password" autoComplete="off" onChange={(v: string) => setFieldValue("confirm_password", v)} />
                <Form.HelpText>{errors.confirm_password && errors.confirm_password}</Form.HelpText>
            </Form.Group>
            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
            <span className="info">Already have an accout? <a href="/sign-in">{'login'}</a></span>
        </Form>
    )
}