import { FC } from "react";
import { Button, ButtonGroup, Form } from "rsuite";

import "./style.scss"


export interface SignInFormProps {

}

export const SignInForm: FC<SignInFormProps> = (props: SignInFormProps) => {

    return (
        <Form fluid>
            <Form.Group controlId="email">
                <Form.ControlLabel>Email or username</Form.ControlLabel>
                <Form.Control name="email" type="email" />
                <Form.HelpText>This field is required</Form.HelpText>
            </Form.Group>
            <Form.Group controlId="password">
                <Form.ControlLabel>Password</Form.ControlLabel>
                <Form.Control name="password" type="password" autoComplete="off" />
            </Form.Group>
            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary">Submit</Button>
                    <Button appearance="ghost">Cancel</Button>
                </ButtonGroup>
            </Form.Group>
            <span className="info">Don't have an accout? <a href="/registration">{'Sign up'}</a></span>
        </Form>
    )
}