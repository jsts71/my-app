import { TicketType, ticketTypeInitialValues, ticketTypeValidationSchema } from 'models/ticket';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { ObjectSchema } from 'yup';
import { useCreateTicketTypeMutation, useGetTicketTypeByIdMutation, useUpdateTicketTypeMutation } from '../redux/apis';
import { useEffect } from 'react';

export interface TicketTypesFormHookProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export interface TicketTypesFormHookInterface {
    validationSchema: ObjectSchema<TicketType>
    initialValues: TicketType
    onSubmit: (values: TicketType) => void
    isLoading: boolean
    isSuccess: boolean
    isError: boolean
}

export const useTicketTypeFormHook = (props: TicketTypesFormHookProps): TicketTypesFormHookInterface => {

    const { id, setView } = props
    const [initialValues, setInitialValues] = useState(ticketTypeInitialValues)
    const [validationSchema, setValidationSchema] = useState(ticketTypeValidationSchema)
    const [createTicketTypes, { data, isLoading, isSuccess, isError }] = useCreateTicketTypeMutation()
    const [getTicketTypeById, { data: getByIdData, isLoading: getByIdIsLoading, isSuccess: getByIdIsSuccess }] = useGetTicketTypeByIdMutation()
    const [updateTicketType, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateTicketTypeMutation()

    useEffect(() => {
        id && getTicketTypeById(id)
    }, [id])

    useEffect(() => {
        id && setInitialValues(getByIdData)
    }, [getByIdIsSuccess])

    useEffect(() => {
        isSuccess && setView('list')
        isError && toast.error("Something went wrong! Failed to create new event type!")
        isSuccess && toast.success("Event type created successfully.")
    }, [isLoading, isSuccess, isError])

    useEffect(() => {
        updateIsSuccess && setView('view')
        updateIsError && toast.error("Something went wrong! Failed to update this event type!")
        updateIsSuccess && toast.success("Event type updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])

    const onSubmit = (values: TicketType) => {
        id ?
            updateTicketType(values)
            :
            createTicketTypes(values)
    }

    return {
        validationSchema,
        initialValues,
        onSubmit,
        isLoading,
        isSuccess,
        isError
    }
}