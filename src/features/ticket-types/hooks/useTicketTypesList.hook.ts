import { TicketFilter } from 'models/ticket';
import { useEffect, useState } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { useGetAllTicketTypesMutation, useRemoveTicketTypeByIdMutation } from '../redux/apis';

export interface TicketTypesFormHookProps {

}

export interface TicketTypesFormHookInterface {
    onClickRemove: (id: number) => void
    isLoading: boolean
    isSuccess: boolean
    activePage: number
    setActivePage: (value: number) => void
    data: any
}

export const useTicketTypeListHook = (props: TicketTypesFormHookProps): TicketTypesFormHookInterface => {

    const [activePage, setActivePage] = useState(1);
    const [getAllTicketTypes, { data, isLoading, isSuccess }] = useGetAllTicketTypesMutation()
    const [removeTicketTypeById, { data: removedData, isLoading: removedIsLoading, isSuccess: removedIsSuccess, isError: removeIsError }] = useRemoveTicketTypeByIdMutation()
    var filter = new TicketFilter();

    const onClickRemove = (id: number) => {
        confirmAlert({
            title: 'Remove ticket type ' + id,
            message: 'Are you sure to remove this ticket type?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => removeTicketTypeById(id)
                },
                {
                    label: 'No',
                    onClick: () => toast.info('You cancel to delete!')
                }
            ]
        });
    };

    useEffect(() => {
        filter.setPage(activePage)
        getAllTicketTypes(filter.to_string())

    }, [activePage])

    useEffect(() => {
        filter.setPage(activePage)
        removedIsSuccess && toast.success('Ticket type removed successfully!')
        !removedIsSuccess ?? getAllTicketTypes(filter.to_string()); setActivePage(1); filter.setPage(activePage); getAllTicketTypes(filter.to_string())
        removeIsError && toast.error('Ticket type failed to remove!')
    }, [removedIsSuccess, removedIsLoading, removeIsError])

    return {
        onClickRemove,
        isLoading,
        isSuccess,
        activePage,
        setActivePage,
        data
    }
}