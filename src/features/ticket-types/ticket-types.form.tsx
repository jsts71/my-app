import { FC, useEffect } from "react";
import { Button, ButtonGroup, Form, Input, InputNumber, Loader, Message, toaster, Toggle } from "rsuite";
import { useFormik } from 'formik';
import { useToaster } from 'rsuite/toaster';

import { RS } from "components/regular";
import { useTicketTypeFormHook } from "./hooks/useTicketTypesForm.hook";
import { useCreateTicketTypeMutation } from "./redux/apis";
import { TicketType } from 'models/ticket';

import "./style.scss"
import { DefaultMessage } from "components/message/defaultMessage";

import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

export interface TicketTypesFormProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export const TicketTypesForm: FC<TicketTypesFormProps> = (props: TicketTypesFormProps) => {

    const { id, setView } = props
    const toster = useToaster()

    const { validationSchema, initialValues, onSubmit, isLoading, isSuccess, isError } = useTicketTypeFormHook({ id, setView })


    const formik = useFormik({ initialValues, onSubmit, validateOnChange: false, validationSchema });


    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        id && setValues(initialValues)
    }, [initialValues])

    return (
        isLoading ? <Loader center content="loading" /> :
            <Form fluid>
                <Form.Group controlId="name">
                    <Form.ControlLabel>Name <RS /></Form.ControlLabel>
                    <Form.Control name="name" onChange={(v: string) => setFieldValue("name", v)} value={values?.name} />
                    <Form.HelpText>{errors.name && errors.name}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="discount_percent">
                    <Form.ControlLabel>Discount in percent <RS /></Form.ControlLabel>
                    <InputNumber max={100} min={0} name="ticket_cost" type="number" step={0.1} onChange={(v: number | string) => setFieldValue("discount_percent", v)} value={values?.discount_percent} postfix="%" />
                    <Form.HelpText>{errors.discount_percent && errors.discount_percent}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.ControlLabel>Ticket type description <RS /></Form.ControlLabel>
                    <Input as="textarea" rows={5} name="description" onChange={(v: string) => setFieldValue("description", v)} value={values?.description} />
                    <Form.HelpText>{errors.description && errors.description}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="is_volunteer_type">
                    <Form.ControlLabel>Ticket type is volunteer? </Form.ControlLabel>
                    <Toggle checkedChildren="Yes" unCheckedChildren="No" onChange={(v: boolean) => setFieldValue("is_volunteer_type", v)} checked={values?.is_volunteer_type} />
                    <Form.HelpText>{errors.is_volunteer_type && errors.is_volunteer_type}</Form.HelpText>
                </Form.Group>

                <Form.Group>
                    <ButtonGroup style={{ marginTop: 12 }} justified>
                        <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                        <Button appearance="ghost" onClick={(e) => setView('list')}>Cancel</Button>
                    </ButtonGroup>
                </Form.Group>
            </Form>
    )
}