import rest_api from 'app/rest-api';
import { TicketType } from 'models/ticket';

const ticketTypeApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createTicketType: build.mutation({
            query: (payload: TicketType) => ({ method: "POST", url: '/ticket/ticket-type/', data: payload })
        }),
        updateTicketType: build.mutation({
            query: (payload: TicketType) => ({ method: "PUT", url: '/ticket/ticket-type/'+payload.id+'/', data: payload })
        }),
        getAllTicketTypes: build.mutation({
            query: (query: string) => ({ method: "GET", url: '/ticket/ticket-type/?' + query })
        }),
        getTicketTypeById: build.mutation({
            query: (id) => ({ method: "GET", url: '/ticket/ticket-type/' + id + '/' })
        }),
        removeTicketTypeById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/ticket/ticket-type/' + id })
        }),
        getTicketTypesByCategory: build.query({
            query: (category) => ({ method: "GET", url: '/ticket/ticket-type', params: { category: category } })
        }),


    })
})


export const {
    useCreateTicketTypeMutation,
    useUpdateTicketTypeMutation,
    useGetAllTicketTypesMutation,
    useGetTicketTypeByIdMutation,
    useRemoveTicketTypeByIdMutation,
    useGetTicketTypesByCategoryQuery

} = ticketTypeApi
export default ticketTypeApi