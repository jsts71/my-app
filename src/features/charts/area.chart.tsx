import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';


const data = [
    {
      name: 'Page A',
      uv: 4,
      pv: 2,
      amt: 2,
    },
    {
      name: 'Page B',
      uv: 10,
      pv: 13,
      amt: 12,
    },
    {
      name: 'Page C',
      uv: 20,
      pv: 98,
      amt: 20,
    },
    {
      name: 'Page D',
      uv: 27,
      pv: 39,
      amt: 20,
    },
    {
      name: 'Page E',
      uv: 18,
      pv: 48,
      amt: 21,
    },
    {
      name: 'Page F',
      uv: 23,
      pv: 38,
      amt: 20,
    },
    {
      name: 'Page G',
      uv: 34,
      pv: 43,
      amt: 20,
    },
  ];

export const AreaChartComponent = () => {
    return (
        <ResponsiveContainer width="100%" height="50%" >
            <AreaChart
                width={500}
                height={400}
                data={data}
                margin={{
                    top: 10,
                    right: 30,
                    left: 0,
                    bottom: 0,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Area type="monotone" dataKey="uv" stroke="#8884d8" fill="#05717d" />
            </AreaChart>
        </ResponsiveContainer>
    )
}