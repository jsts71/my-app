import { FC, useEffect } from "react";
import { ButtonToolbar, FlexboxGrid, IconButton, List, Loader, } from "rsuite";

import "./style.scss"
import { useGetVolunteerTicketByIdMutation } from "./redux/apis";
import EditIcon from '@rsuite/icons/Edit';
import { convertFromRaw, EditorState } from "draft-js";
import { DefaultDraftView } from "components/rich-text-editor/defaultDraftJsView";

export interface VolunteerTicketViewProps {
    id: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void;
    setId: (value: number) => void;
}

const styleleft = {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    minHeight: '60px'
};
const styleRight = {
    display: 'flex',
    justifyContent: 'right',
    alignItems: 'center',
    minHeight: '60px'
};

export const VolunteerTicketsView: FC<VolunteerTicketViewProps> = (props: VolunteerTicketViewProps) => {

    const { id, setId, setView } = props

    const [getVolunteerTicketById, { data, isLoading, isSuccess }] = useGetVolunteerTicketByIdMutation()

    useEffect(() => {
        id && getVolunteerTicketById(id)
    }, [id])


    return (
        <>{isLoading && <Loader center content="loading" />}
            {isSuccess &&
                <>
                    < List hover>
                        <List.Item key={0} index={0}>
                            <FlexboxGrid>
                                <FlexboxGrid.Item colspan={24} style={styleRight}>
                                    <ButtonToolbar>
                                        <IconButton circle icon={<EditIcon />} color="green" appearance="subtle" onClick={() => { setView('update'); setId(id) }} />
                                        {/* <IconButton circle icon={<TrashIcon />} color="red" appearance="subtle" onClick={() => { console.log('remove') }} /> */}
                                    </ButtonToolbar>
                                </FlexboxGrid.Item>
                            </FlexboxGrid>
                        </List.Item>
                        {Object.keys(data).map((key: string, index: number) =>
                            <List.Item key={key} index={index}>

                                {key}
                                :
                                {key == 'ticket_template' || key == 'agreement_template' || key == 'introduction_template' || key == 'whs_template' || key == 'terms_n_condition' || key == 'note'?
                                    <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data[key])))} />
                                    :
                                    data[key]
                                }
                            </List.Item>
                        )}
                    </List>
                </>
            }
        </>
    )
}