import { FC, useEffect, useState } from "react";
import { Button, ButtonGroup, Form, Input, InputNumber, Loader, Message, SelectPicker, toaster, Toggle } from "rsuite";
import { useFormik } from 'formik';

import { RS } from "components/regular";
import { useVolunteerTicketFormHook } from "./hooks/useVolunteerTicketForm.hook";
import "./style.scss"
import { DefaultDraftEditor } from "components/rich-text-editor/defaultDraftJsEditor";
import { convertFromRaw, convertToRaw, EditorState } from "draft-js";

export interface VolunteerTicketFormProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export const VolunteerTicketForm: FC<VolunteerTicketFormProps> = (props: VolunteerTicketFormProps) => {

    const { id, setView } = props
    const { validationSchema, initialValues, onSubmit, isLoading, isSuccess, isError, events, ticketTypes } = useVolunteerTicketFormHook({ id, setView })
    const formik = useFormik({ initialValues, onSubmit, validateOnChange: false, validationSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    const [agreementState, setAgreementState] = useState(() => EditorState.createEmpty());
    const [introductionState, setIntroductionState] = useState(() => EditorState.createEmpty());
    const [whsState, setWhsState] = useState(() => EditorState.createEmpty());
    const [ticketState, setTicketState] = useState(() => EditorState.createEmpty());
    const [termsAndConditionState, setTermsAndConditionStateState] = useState(() => EditorState.createEmpty());
    const [noteState, setNoteState] = useState(() => EditorState.createEmpty());

    useEffect(() => {
        setFieldValue("agreement_template", JSON.stringify(convertToRaw(agreementState.getCurrentContent())))
    }, [agreementState])

    useEffect(() => {
        setFieldValue("introduction_template", JSON.stringify(convertToRaw(introductionState.getCurrentContent())))
    }, [introductionState])

    useEffect(() => {
        setFieldValue("whs_template", JSON.stringify(convertToRaw(whsState.getCurrentContent())))
    }, [whsState])

    useEffect(() => {
        setFieldValue("ticket_template", JSON.stringify(convertToRaw(ticketState.getCurrentContent())))
    }, [ticketState])

    useEffect(() => {
        setFieldValue("terms_n_condition", JSON.stringify(convertToRaw(termsAndConditionState.getCurrentContent())))
    }, [termsAndConditionState])

    useEffect(() => {
        setFieldValue("note", JSON.stringify(convertToRaw(noteState.getCurrentContent())))
    }, [noteState])


    useEffect(() => {
        initialValues?.agreement_template.length && setAgreementState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.agreement_template))))
    }, [initialValues?.agreement_template])

    useEffect(() => {
        initialValues?.introduction_template.length && setIntroductionState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.introduction_template))))
    }, [initialValues?.introduction_template])

    useEffect(() => {
        initialValues?.whs_template.length && setWhsState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.whs_template))))
    }, [initialValues?.whs_template])

    useEffect(() => {
        initialValues?.ticket_template.length && setTicketState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.ticket_template))))
    }, [initialValues?.ticket_template])

    useEffect(() => {
        initialValues?.terms_n_condition.length && setTermsAndConditionStateState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.terms_n_condition))))
    }, [initialValues?.terms_n_condition])

    useEffect(() => {
        initialValues?.note.length && setNoteState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.note))))
    }, [initialValues?.note])

    useEffect(() => {
        id && setValues(initialValues)
    }, [initialValues])

    return (
        isLoading ? <Loader center content="loading" /> :
            <Form fluid>
                <Form.Group controlId="event">
                    <Form.ControlLabel>Volunteer ticket name <RS /></Form.ControlLabel>
                    <Form.Control name="name" onChange={(v: string) => setFieldValue("name", v)} value={values?.name} />
                    <Form.HelpText>{errors.name && errors.name}</Form.HelpText>
                </Form.Group>
                <Form.Group controlId="event">
                    <Form.ControlLabel>Volunteer ticket event <RS /></Form.ControlLabel>
                    <SelectPicker block name="event" data={events} onChange={(v: string | number | null) => { console.log(v); setFieldValue("event", v) }} value={values?.event} />
                    <Form.HelpText>{errors.event && errors.event}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="ticket_type">
                    <Form.ControlLabel>Volunteer ticket type <RS /></Form.ControlLabel>
                    <SelectPicker block name="ticket_type" data={ticketTypes} onChange={(v: string | number | null) => setFieldValue("ticket_type", v)} value={values?.ticket_type} />
                    <Form.HelpText>{errors.ticket_type && errors.ticket_type}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="aggrement_template">
                    <Form.ControlLabel>Aggrement template <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={agreementState} setEditorState={(value: EditorState) => setAgreementState(value)} />
                    {/* <Input as="textarea" rows={5} name="agreement_template" onChange={(v: string) => setFieldValue("agreement_template", v)} value={values?.agreement_template} /> */}
                    <Form.HelpText>{errors.agreement_template && errors.agreement_template}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="introduction_template">
                    <Form.ControlLabel>Introduction template <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={introductionState} setEditorState={(value: EditorState) => setIntroductionState(value)} />
                    {/* <Input as="textarea" rows={5} name="introduction_template" onChange={(v: string) => setFieldValue("introduction_template", v)} value={values?.introduction_template} /> */}
                    <Form.HelpText>{errors.introduction_template && errors.introduction_template}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="whs_template">
                    <Form.ControlLabel>Whs template <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={whsState} setEditorState={(value: EditorState) => setWhsState(value)} />
                    {/* <Input as="textarea" rows={5} name="whs_template" onChange={(v: string) => setFieldValue("whs_template", v)} value={values?.whs_template} /> */}
                    <Form.HelpText>{errors.whs_template && errors.whs_template}</Form.HelpText>
                </Form.Group>

                {/* <Form.Group controlId="payment_method">
                    <Form.ControlLabel>payment method <RS /></Form.ControlLabel>
                    <SelectPicker block name="payment_method" data={[]} onChange={(v: number | null) => setFieldValue("payment_method", v)} value={values?.payment_method} />
                    <Form.HelpText>{errors.payment_method && errors.payment_method}</Form.HelpText>
                </Form.Group> */}

                <Form.Group controlId="ticket_template">
                    <Form.ControlLabel>Ticket template <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={ticketState} setEditorState={(value: EditorState) => setTicketState(value)} />
                    {/* <Input as="textarea" rows={5} name="ticket_template" onChange={(v: string) => setFieldValue("ticket_template", v)} value={values?.ticket_template} /> */}
                    <Form.HelpText>{errors.ticket_template && errors.ticket_template}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="terms_n_condition">
                    <Form.ControlLabel>Terms and condition <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={termsAndConditionState} setEditorState={(value: EditorState) => setTermsAndConditionStateState(value)} />
                    {/* <Input as="textarea" rows={5} name="ticket_template" onChange={(v: string) => setFieldValue("ticket_template", v)} value={values?.ticket_template} /> */}
                    <Form.HelpText>{errors.terms_n_condition && errors.terms_n_condition}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="note">
                    <Form.ControlLabel>Note <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={noteState} setEditorState={(value: EditorState) => setNoteState(value)} />
                    {/* <Input as="textarea" rows={5} name="ticket_template" onChange={(v: string) => setFieldValue("ticket_template", v)} value={values?.ticket_template} /> */}
                    <Form.HelpText>{errors.note && errors.note}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="link" hidden>
                    <Form.ControlLabel>Ticket link <RS /></Form.ControlLabel>
                    <Form.Control name="link" onChange={(v: string) => setFieldValue("link", v)} value={values?.link} defaultValue='no value' />
                    <Form.HelpText>{errors.link && errors.link}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="public_key">
                    <Form.ControlLabel>Public Key <RS /></Form.ControlLabel>
                    <Form.Control name="public_key" onChange={(v: string) => setFieldValue("public_key", v)} value={values?.public_key} />
                    <Form.HelpText>{errors.public_key && errors.public_key}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="secret_key">
                    <Form.ControlLabel>Secret key <RS /></Form.ControlLabel>
                    <Form.Control name="secret_key" onChange={(v: string) => setFieldValue("secret_key", v)} value={values?.secret_key} />
                    <Form.HelpText>{errors.secret_key && errors.secret_key}</Form.HelpText>
                </Form.Group>

                <Form.Group>
                    <ButtonGroup style={{ marginTop: 12 }} justified>
                        <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                        <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                    </ButtonGroup>
                </Form.Group>
            </Form>
    )
}