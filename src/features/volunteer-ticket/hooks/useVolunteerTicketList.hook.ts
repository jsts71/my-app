import { TicketFilter } from 'models/ticket';
import { useEffect, useState } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { useGetAllVolunteerTicketsMutation, useRemoveVolunteerTicketByIdMutation } from '../redux/apis';

export interface VolunteerTicketFormHookProps {

}

export interface VolunteerTicketFormHookInterface {
    onClickRemove: (id: number) => void
    isLoading: boolean
    isSuccess: boolean
    activePage: number
    setActivePage: (value: number) => void
    data: any
}

export const useVolunteerTicketListHook = (props: VolunteerTicketFormHookProps): VolunteerTicketFormHookInterface => {

    const [activePage, setActivePage] = useState(1);
    const [getAllVolunteerTicket, { data, isLoading, isSuccess }] = useGetAllVolunteerTicketsMutation()
    const [removeVolunteerTicketById, { data: removedData, isLoading: removedIsLoading, isSuccess: removedIsSuccess, isError: removeIsError }] = useRemoveVolunteerTicketByIdMutation()
    var filter = new TicketFilter();

    const onClickRemove = (id: number) => {
        confirmAlert({
            title: 'Remove ticket type ' + id,
            message: 'Are you sure to remove this ticket type?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => removeVolunteerTicketById(id)
                },
                {
                    label: 'No',
                    onClick: () => toast.info('You cancel to delete!')
                }
            ]
        });
    };

    useEffect(() => {
        filter.setPage(activePage)
        getAllVolunteerTicket(filter.to_string())

    }, [activePage])

    useEffect(() => {
        filter.setPage(activePage)
        removedIsSuccess && toast.success('Ticket type removed successfully!')
        !removedIsSuccess ?? getAllVolunteerTicket(filter.to_string()); setActivePage(1); filter.setPage(activePage); getAllVolunteerTicket(filter.to_string())
        removeIsError && toast.error('Ticket type failed to remove!')
    }, [removedIsSuccess, removedIsLoading, removeIsError])

    return {
        onClickRemove,
        isLoading,
        isSuccess,
        activePage,
        setActivePage,
        data
    }
}