import { VolunteerTicket, VolunteerTicketInitialValues, VolunteerTicketValidationSchema } from 'models/ticket';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { ObjectSchema } from 'yup';
import { useCreateVolunteerTicketMutation, useGetVolunteerTicketByIdMutation, useUpdateVolunteerTicketMutation } from '../redux/apis';
import { useEffect } from 'react';
import { useGetAllEventMutation } from 'features/event-create/redux/apis';
import { ItemDataType } from 'rsuite/esm/@types/common';
import { WhatEvent } from 'models/event';
import _ from 'lodash';
import { useGetAllTicketTypesMutation } from 'features/ticket-types/redux/apis';
import { TicketType } from 'models/ticket';
import { decode as base64_decode, encode as base64_encode } from 'base-64';
import { encodedd } from './ab';



export interface VolunteerTicketFormHookProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export interface VolunteerTicketFormHookInterface {
    validationSchema: ObjectSchema<VolunteerTicket>
    initialValues: VolunteerTicket
    onSubmit: (values: VolunteerTicket) => void
    isLoading: boolean
    isSuccess: boolean
    isError: boolean
    events: ItemDataType[]
    ticketTypes: ItemDataType[]

}

export const useVolunteerTicketFormHook = (props: VolunteerTicketFormHookProps): VolunteerTicketFormHookInterface => {

    const { id, setView } = props
    const [initialValues, setInitialValues] = useState(VolunteerTicketInitialValues)
    const [validationSchema, setValidationSchema] = useState(VolunteerTicketValidationSchema)


    const [createVolunteerTicket, { data, isLoading, isSuccess, isError }] = useCreateVolunteerTicketMutation()
    const [getVolunteerTicketById, { data: getByIdData, isLoading: getByIdIsLoading, isSuccess: getByIdIsSuccess }] = useGetVolunteerTicketByIdMutation()
    const [updateVolunteerTicket, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateVolunteerTicketMutation()

    const [getAllEvent, { data: eventData, isLoading: eventIsLoading, isSuccess: eventIsSuccess }] = useGetAllEventMutation()
    const [events, setEvents] = useState<ItemDataType[]>([])

    const [getAllTicketType, { data: ticketTypesData, isLoading: ticketTypesIsLoading, isSuccess: ticketTypesIsSuccess }] = useGetAllTicketTypesMutation()
    const [ticketTypes, setTicketTypes] = useState<ItemDataType[]>([])


    useEffect(() => {
        id && getVolunteerTicketById(id)
    }, [id])

    useEffect(() => {
        id && setInitialValues(getByIdData)
    }, [getByIdIsSuccess])

    useEffect(() => {
        isSuccess && setView('list')
        isError && toast.error("Something went wrong! Failed to create new event Category!")
        isSuccess && toast.success("Event Category created successfully.")
    }, [isLoading, isSuccess, isError])

    useEffect(() => {
        updateIsSuccess && setView('view')
        updateIsError && toast.error("Something went wrong! Failed to update this event Category!")
        updateIsSuccess && toast.success("Event Category updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])




    useEffect(() => {
        let pageLimit = parseInt(process.env.REACT_APP_PAGINATION_LIMIT ?? '10')
        let eventPicker: ItemDataType[] = []
        const callbackFunction = async (page: number) => await getAllEvent('page=' + page).then((response: any) => {
            let responsePicker = _.map(response.data.results, (result: WhatEvent) => { return { value: result.id, label: result.title } })
            eventPicker = [...eventPicker, ...responsePicker]
            setEvents(eventPicker)
            if (response.data.count > pageLimit) {
                let totalPage: number = Math.ceil(response.data.count / pageLimit)
                if (page <= totalPage) {
                    page = page + 1
                    page <= totalPage && callbackFunction(page)
                }
            }
        })
        callbackFunction(1)
    }, [])


    useEffect(() => {
        let pageLimit = parseInt(process.env.REACT_APP_PAGINATION_LIMIT ?? '10')
        let ticketTypePicker: ItemDataType[] = []
        const callbackFunction = async (page: number) => await getAllTicketType('page=' + page).then((response: any) => {
            let responsePicker = _.map(response.data.results, (result: TicketType) => { return { value: result.id, label: result.name } })
            ticketTypePicker = [...ticketTypePicker, ...responsePicker]
            setTicketTypes(ticketTypePicker)
            if (response.data.count > pageLimit) {
                let totalPage: number = Math.ceil(response.data.count / pageLimit)
                if (page <= totalPage) {
                    page = page + 1
                    page <= totalPage && callbackFunction(page)
                }
            }
        })
        callbackFunction(1)
    }, [])

    const onSubmit = (values: VolunteerTicket) => {

        // values.agreement_template = base64_encode(values.agreement_template);
        // let decoded = base64_decode('YOUR_ENCODED_STRING');
        // // values.agreement_template = JSON.stringify(values.agreement_template)
        // let a = encodeURIComponent(values.agreement_template)
        // console.log(window.atob(values.agreement_template));
        id ?
            updateVolunteerTicket(values)
            :
            createVolunteerTicket(values)
    }

    return {
        validationSchema,
        initialValues,
        onSubmit,
        isLoading,
        isSuccess,
        isError,
        events,
        ticketTypes
    }
}