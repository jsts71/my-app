import rest_api from 'app/rest-api';
import { VolunteerTicket } from 'models/ticket';

const volunteerTicketApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createVolunteerTicket: build.mutation({
            query: (payload: VolunteerTicket) => ({ method: "POST", url: '/ticket/volunteer-ticket/', data: payload })
        }),
        updateVolunteerTicket: build.mutation({
            query: (payload: VolunteerTicket) => ({ method: "PUT", url: '/ticket/volunteer-ticket/' + payload.id + '/', data: payload })
        }),
        getAllVolunteerTickets: build.mutation({
            query: (filter: string) => ({ method: "GET", url: '/ticket/volunteer-ticket/?' + filter })
        }),
        getVolunteerTicketById: build.mutation({
            query: (id) => ({ method: "GET", url: '/ticket/volunteer-ticket/' + id })
        }),
        removeVolunteerTicketById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/ticket/volunteer-ticket/' + id })
        })
    })
})


export const {
    useCreateVolunteerTicketMutation,
    useUpdateVolunteerTicketMutation,
    useGetVolunteerTicketByIdMutation,
    useGetAllVolunteerTicketsMutation,
    useRemoveVolunteerTicketByIdMutation,

} = volunteerTicketApi

export default volunteerTicketApi