import { FC, useEffect } from "react";
import { Button, ButtonGroup, Form, Input, InputNumber, Loader, Message, toaster, Toggle } from "rsuite";
import { useFormik } from 'formik';
import { useToaster } from 'rsuite/toaster';

import { RS } from "components/regular";
import { useEventCategoryFormHook } from "./hooks/useEventCategoryForm.hook";

import "./style.scss"

import 'react-toastify/dist/ReactToastify.css';

export interface EventCategoryFormProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export const EventCategoryForm: FC<EventCategoryFormProps> = (props: EventCategoryFormProps) => {

    const { id, setView } = props

    const { validationSchema, initialValues, onSubmit, isLoading, isSuccess, isError } = useEventCategoryFormHook({ id, setView })

    const formik = useFormik({ initialValues, onSubmit, validateOnChange: false, validationSchema });

    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        id && setValues(initialValues)
    }, [initialValues])

    return (
        isLoading ? <Loader center content="loading" /> :
            <Form fluid>
                <Form.Group controlId="name">
                    <Form.ControlLabel>Name <RS /></Form.ControlLabel>
                    <Form.Control name="name" onChange={(v: string) => setFieldValue("name", v)} value={values?.name} />
                    <Form.HelpText>{errors.name && errors.name}</Form.HelpText>
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.ControlLabel>Event category description <RS /></Form.ControlLabel>
                    <Input as="textarea" rows={5} name="description" onChange={(v: string) => setFieldValue("description", v)} value={values?.description} />
                    <Form.HelpText>{errors.description && errors.description}</Form.HelpText>
                </Form.Group>

                <Form.Group>
                    <ButtonGroup style={{ marginTop: 12 }} justified>
                        <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                        <Button appearance="ghost" onClick={(e) => setView('list')}>Cancel</Button>
                    </ButtonGroup>
                </Form.Group>
            </Form>
    )
}