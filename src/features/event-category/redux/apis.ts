import rest_api from 'app/rest-api';
import { EventCategory } from 'models/event';

const EventCategoryApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createEventCategory: build.mutation({
            query: (payload: EventCategory) => ({ method: "POST", url: '/event/event-category/', data: payload })
        }),
        updateEventCategory: build.mutation({
            query: (payload: EventCategory) => ({ method: "PUT", url: '/event/event-category/'+payload.id+'/', data: payload })
        }),
        getAllEventCategory: build.mutation({
            query: (query: string) => ({ method: "GET", url: '/event/event-category/?' + query })
        }),
        getEventCategoryById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/event-category/' + id + '/' })
        }),
        removeEventCategoryById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/event-category/' + id })
        }),
        getEventCategorysByCategory: build.query({
            query: (category) => ({ method: "GET", url: '/event/event-category', params: { category: category } })
        }),


    })
})


export const {
    useCreateEventCategoryMutation,
    useUpdateEventCategoryMutation,
    useGetAllEventCategoryMutation,
    useGetEventCategoryByIdMutation,
    useRemoveEventCategoryByIdMutation,
    useGetEventCategorysByCategoryQuery

} = EventCategoryApi
export default EventCategoryApi