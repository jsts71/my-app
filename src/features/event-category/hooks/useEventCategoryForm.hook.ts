import { EventCategory, EventCategoryInitialValues, EventCategoryValidationSchema } from 'models/event';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { ObjectSchema } from 'yup';
import { useCreateEventCategoryMutation, useGetEventCategoryByIdMutation, useUpdateEventCategoryMutation } from '../redux/apis';
import { useEffect } from 'react';

export interface EventCategoryFormHookProps {
    id?: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
}

export interface EventCategoryFormHookInterface {
    validationSchema: ObjectSchema<EventCategory>
    initialValues: EventCategory
    onSubmit: (values: EventCategory) => void
    isLoading: boolean
    isSuccess: boolean
    isError: boolean
}

export const useEventCategoryFormHook = (props: EventCategoryFormHookProps): EventCategoryFormHookInterface => {

    const { id, setView } = props
    const [initialValues, setInitialValues] = useState(EventCategoryInitialValues)
    const [validationSchema, setValidationSchema] = useState(EventCategoryValidationSchema)
    const [createEventCategory, { data, isLoading, isSuccess, isError }] = useCreateEventCategoryMutation()
    const [getEventCategoryById, { data: getByIdData, isLoading: getByIdIsLoading, isSuccess: getByIdIsSuccess }] = useGetEventCategoryByIdMutation()
    const [updateEventCategory, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateEventCategoryMutation()

    useEffect(() => {
        id && getEventCategoryById(id)
    }, [id])

    useEffect(() => {
        id && setInitialValues(getByIdData)
    }, [getByIdIsSuccess])

    useEffect(() => {
        isSuccess && setView('list')
        isError && toast.error("Something went wrong! Failed to create new event Category!")
        isSuccess && toast.success("Event Category created successfully.")
    }, [isLoading, isSuccess, isError])

    useEffect(() => {
        updateIsSuccess && setView('view')
        updateIsError && toast.error("Something went wrong! Failed to update this event Category!")
        updateIsSuccess && toast.success("Event Category updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])

    const onSubmit = (values: EventCategory) => {
        id ?
            updateEventCategory(values)
            :
            createEventCategory(values)
    }

    return {
        validationSchema,
        initialValues,
        onSubmit,
        isLoading,
        isSuccess,
        isError
    }
}