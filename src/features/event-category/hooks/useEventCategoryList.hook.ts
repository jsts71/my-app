import { TicketFilter } from 'models/ticket';
import { useEffect, useState } from 'react';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { useGetAllEventCategoryMutation, useRemoveEventCategoryByIdMutation } from '../redux/apis';

export interface EventCategoryFormHookProps {

}

export interface EventCategoryFormHookInterface {
    onClickRemove: (id: number) => void
    isLoading: boolean
    isSuccess: boolean
    activePage: number
    setActivePage: (value: number) => void
    data: any
}

export const useEventCategoryListHook = (props: EventCategoryFormHookProps): EventCategoryFormHookInterface => {

    const [activePage, setActivePage] = useState(1);
    const [getAllEventCategory, { data, isLoading, isSuccess }] = useGetAllEventCategoryMutation()
    const [removeEventCategoryById, { data: removedData, isLoading: removedIsLoading, isSuccess: removedIsSuccess, isError: removeIsError }] = useRemoveEventCategoryByIdMutation()
    var filter = new TicketFilter();

    const onClickRemove = (id: number) => {
        confirmAlert({
            title: 'Remove ticket type ' + id,
            message: 'Are you sure to remove this ticket type?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => removeEventCategoryById(id)
                },
                {
                    label: 'No',
                    onClick: () => toast.info('You cancel to delete!')
                }
            ]
        });
    };

    useEffect(() => {
        filter.setPage(activePage)
        getAllEventCategory(filter.to_string())

    }, [activePage])

    useEffect(() => {
        filter.setPage(activePage)
        removedIsSuccess && toast.success('Ticket type removed successfully!')
        !removedIsSuccess ?? getAllEventCategory(filter.to_string()); setActivePage(1); filter.setPage(activePage); getAllEventCategory(filter.to_string())
        removeIsError && toast.error('Ticket type failed to remove!')
    }, [removedIsSuccess, removedIsLoading, removeIsError])

    return {
        onClickRemove,
        isLoading,
        isSuccess,
        activePage,
        setActivePage,
        data
    }
}