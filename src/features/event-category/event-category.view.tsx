import { FC, useEffect } from "react";
import { ButtonToolbar, FlexboxGrid, IconButton, List, Loader, } from "rsuite";

import "./style.scss"
import { useGetEventCategoryByIdMutation } from "./redux/apis";
import EditIcon from '@rsuite/icons/Edit';
import TrashIcon from '@rsuite/icons/Trash';
import EyeCloseIcon from '@rsuite/icons/EyeClose';

export interface EventCategoryViewProps {
    id: number;
    setView: (value: 'new' | 'list' | 'view' | 'update') => void;
    setId: (value: number) => void;
}

const styleleft = {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    minHeight: '60px'
};
const styleRight = {
    display: 'flex',
    justifyContent: 'right',
    alignItems: 'center',
    minHeight: '60px'
};

export const EventCategoryView: FC<EventCategoryViewProps> = (props: EventCategoryViewProps) => {

    const { id, setId, setView } = props

    const [getEventCategoryById, { data, isLoading, isSuccess }] = useGetEventCategoryByIdMutation()

    useEffect(() => {
        id && getEventCategoryById(id)
    }, [id])

    console.log(data)

    return (
        <>{isLoading && <Loader center content="loading" />}
            {isSuccess &&
                <>
                    < List hover>
                        <List.Item key={0} index={0}>
                            <FlexboxGrid>
                                <FlexboxGrid.Item colspan={24} style={styleRight}>
                                    <ButtonToolbar>
                                        <IconButton circle icon={<EditIcon />} color="green" appearance="subtle" onClick={() => { setView('update'); setId(id) }} />
                                        {/* <IconButton circle icon={<TrashIcon />} color="red" appearance="subtle" onClick={() => { console.log('remove') }} /> */}
                                    </ButtonToolbar>
                                </FlexboxGrid.Item>
                            </FlexboxGrid>
                        </List.Item>
                        {Object.keys(data).map((key: string, index: number) =>
                            <List.Item key={key} index={index}>
                                <FlexboxGrid>
                                    <FlexboxGrid.Item colspan={8} style={styleleft}>
                                        {key == 'id' && 'ID'}
                                        {key == 'name' && 'Name'}
                                        {key == 'description' && 'Description'}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={1} style={styleleft}>:</FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={15} style={styleleft}>
                                        {data[key]}
                                    </FlexboxGrid.Item>
                                </FlexboxGrid>
                            </List.Item>
                        )}
                    </List>
                </>
            }
        </>
    )
}