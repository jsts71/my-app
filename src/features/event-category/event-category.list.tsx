import { FC } from "react";
import { List, FlexboxGrid, Loader, ButtonToolbar, IconButton } from 'rsuite';

import { EventCategory } from "models/event";
import EditIcon from '@rsuite/icons/Edit';
import TrashIcon from '@rsuite/icons/Trash';
import EyeCloseIcon from '@rsuite/icons/EyeClose';
import { DefaultPagination } from "components/pagination/defaultPagination";
import { useEventCategoryListHook } from "./hooks/useEventCategoryList.hook";
import "./style.scss"

export interface EventCategoryListProps {
    setView: (value: 'new' | 'list' | 'view' | 'update') => void
    setId: (value: number) => void
}

const styleCenter = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '60px'
};

export const EventCategoryList: FC<EventCategoryListProps> = (props: EventCategoryListProps) => {

    const { setView, setId } = props
    const { onClickRemove, isLoading, isSuccess, activePage, setActivePage, data } = useEventCategoryListHook({})

    return (
        <>
            {isLoading && <Loader center content="loading" />}
            <>
                <List>
                    <List.Item key={0} index={0}>
                        <FlexboxGrid style={{ color: "green" }}>
                            <FlexboxGrid.Item colspan={2} style={styleCenter}>ID</FlexboxGrid.Item>
                            <FlexboxGrid.Item colspan={6} style={{ ...styleCenter, justifyContent: 'left' }}>NAME</FlexboxGrid.Item>
                            <FlexboxGrid.Item colspan={10} style={{ ...styleCenter, justifyContent: 'left' }}>DESRIPTION</FlexboxGrid.Item>
                            <FlexboxGrid.Item colspan={6} style={styleCenter}>ACTION</FlexboxGrid.Item>
                        </FlexboxGrid>
                    </List.Item>
                </List>
                {isSuccess &&
                    <List hover>
                        {data?.results?.map((item: EventCategory, index: number) => (
                            <List.Item key={item['id']} index={index + 1}>
                                <FlexboxGrid>
                                    <FlexboxGrid.Item colspan={2} style={styleCenter}>
                                        {item['id']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={6} style={{ ...styleCenter, justifyContent: 'left' }}>
                                        {item['name']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={10} style={{ ...styleCenter, justifyContent: 'left' }}>
                                        {item['description']}
                                    </FlexboxGrid.Item>
                                    <FlexboxGrid.Item colspan={6} style={styleCenter}>
                                        <ButtonToolbar>
                                            <IconButton circle icon={<EditIcon />} color="green" appearance="subtle" onClick={() => { setView('update'); setId(item?.id ?? 0) }} />
                                            <IconButton circle icon={<EyeCloseIcon />} color="blue" appearance="subtle" onClick={() => { setView('view'); setId(item?.id ?? 0) }} />
                                            <IconButton circle icon={<TrashIcon />} color="red" appearance="subtle" onClick={() => { onClickRemove(item?.id ?? 0) }} />
                                        </ButtonToolbar>
                                    </FlexboxGrid.Item>
                                </FlexboxGrid>
                            </List.Item>
                        )) ?? ''}
                    </List>
                }
            </>
            <DefaultPagination
                total={data?.count ?? 0}
                activePage={activePage}
                onChangePage={setActivePage}
            />
        </>
    )
}