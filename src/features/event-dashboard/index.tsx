import { FC, useEffect } from "react";
import { DefaultFilters } from 'components/filters/defaultFilters';
import HeaderWithComponent from 'features/heading/header-with-components';
import { EventDashboardTab, WhatEvent } from "models/event";
import { EventCardPanel } from "components/panel/cardPanel.event";
import { useEventDashboard } from "./hooks/useEventDashboard.hook";
import { useGetAllEventMutation } from "features/event-create/redux/apis";


export interface EventDashboardProps {
    active: EventDashboardTab
}

export const EventDashboard: FC<EventDashboardProps> = (props: EventDashboardProps) => {

    const { active } = props
    const { } = useEventDashboard({ active })

    const [getAllEvent, { data, isLoading, isSuccess, isError }] = useGetAllEventMutation()

    useEffect(() => {
        getAllEvent(null)
    }, [])

    return (
        <>
            <DefaultFilters />
            <HeaderWithComponent h={active + ' events'} s="8" />
            {
                data?.results?.map((element: WhatEvent) => (
                    <>
                        {(element.status == active || active == 'All') && <EventCardPanel header={element.title} data={element} />}
                    </>
                ))
            }
        </>
    )
}