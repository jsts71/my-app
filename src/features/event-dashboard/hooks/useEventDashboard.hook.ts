import { useGetAllEventMutation } from "features/event-create/redux/apis"
import { EventDashboardTab } from "models/event"
import { useEffect } from 'react';

export interface UseEventDashboardProps {
    active: EventDashboardTab
}

export interface UseEventDashboardInterface {

}

export const useEventDashboard = (props: UseEventDashboardProps): UseEventDashboardInterface => {

    const { active } = props
    const [getAllEvent, { data, isLoading, isSuccess, isError }] = useGetAllEventMutation()

    useEffect(() => {
        getAllEvent(null)
    }, [])

    return {

    }
}