import { FC, useState } from 'react';
import { Nav, Navbar, Badge } from "rsuite";
import { AiOutlineUser } from 'react-icons/ai';
import { IoIosNotificationsOutline } from 'react-icons/io';

import './style.scss'
import { DrawerComponent } from 'components/drawer';

export interface ConstHeaderProps {

}

export const ConstHeader: FC<ConstHeaderProps> = (props: ConstHeaderProps) => {

    const [open, setOpen] = useState<boolean>(false)

    return (
        <div className='const-header'>
            <Navbar>
                <Navbar.Brand href="/">HOME</Navbar.Brand>
                <Nav pullRight>
                    <Nav.Item icon={<Badge><IoIosNotificationsOutline size="2em" /></Badge>} onClick={() => setOpen(!open)}></Nav.Item>

                    <Nav.Menu eventKey="20" title="User Name" icon={<AiOutlineUser size="2em" />} placement="bottomEnd">
                        <Nav.Item eventKey="20-1" onClick={() => window.location.href = "/profile"}>User Full Name</Nav.Item>
                        <Nav.Item eventKey="20-2" onClick={() => window.location.href = "/create-event"} >Create Event</Nav.Item>
                        <Nav.Item eventKey="20-3" onClick={() => window.location.href = "/profile"} href="">Manage Event</Nav.Item>
                        <Nav.Item eventKey="20-4" onClick={() => window.location.href = "/profile"} href="">Account Settings</Nav.Item>
                        <Nav.Item eventKey="20-5" onClick={() => window.location.href = "/profile"} href="">Organizer Profile</Nav.Item>
                        <Nav.Item eventKey="20-6" onClick={() => window.location.href = "/profile"} href="">Browse Events</Nav.Item>
                        <Nav.Item eventKey="20-7" onClick={() => window.location.href = "/profile"} href="">My Purchases</Nav.Item>
                        <Nav.Item eventKey="20-8" onClick={() => window.location.href = "/registration"}>Sign Up</Nav.Item>
                        <Nav.Item eventKey="20-9" onClick={() => window.location.href = "/sign-in"} href="/sign-in">Sign In</Nav.Item>
                        <Nav.Item divider />
                        <Nav.Item eventKey="20-10">Logout</Nav.Item>
                    </Nav.Menu>
                </Nav>
            </Navbar>
            <DrawerComponent open={open} setOpen={setOpen} size={'xs'} />
        </div>
    )

}