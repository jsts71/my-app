import _ from "lodash"
import { useState } from "react"
import { Nav, NavItemProps, NavProps } from "rsuite"

export interface DefaultHeaderProps extends NavProps {
    nav_items: NavItemProps[],
    active: string,
}

export const DefaultHeader = (props: DefaultHeaderProps) => {
    const { nav_items, active } = props


    return (
        <div className="default-header">
            <Nav {...props}>
                {nav_items.map((i_props: NavItemProps, index: number) =>
                    <Nav.Item key={index} {..._.omit((i_props), ['click'])} active={active == i_props.children}>
                        {i_props.children}
                    </Nav.Item>
                )}
            </Nav>
        </div>
    )

}