import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from "formik";
import { TicketTab } from "models/event";
import moment from "moment";
import { FC, useEffect } from "react";
import { Button, ButtonGroup, Col, DatePicker, Form, Row, SelectPicker } from "rsuite";
import { useWhenEventForm } from "./hooks/useWhenEventForm.hook";

export interface WhenEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export const WhenEventForm: FC<WhenEventFormProps> = (props: WhenEventFormProps) => {

    const { id, setActive } = props
    const { whenEventSchema, initialValues, onSubmit } = useWhenEventForm({ id, setActive })
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: whenEventSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        setValues(initialValues)
    }, [initialValues])

    console.log(values, values.start_time ? new Date(moment().format("yyyy-MM-DD") + " " + values.start_time) : undefined)


    return (
        <Form fluid>
            <HeaderWithComponent h='Event schedule' s="8" c="green" />
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="start_date">
                        <Form.ControlLabel>Start date</Form.ControlLabel>
                        <DatePicker block name="start_date" format="yyyy-MM-dd" onChange={(v: Date | null) => setFieldValue("start_date", v ? moment(v).format("yyyy-MM-DD") : undefined)} value={values.start_date && new Date(values.start_date)} />
                        <Form.HelpText>{errors.start_date && errors.start_date}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="start_time">
                        <Form.ControlLabel>Stat time</Form.ControlLabel>
                        <DatePicker block name="start_time" format="HH:mm:ss" onChange={(v: Date | null) => { setFieldValue("start_time", v ? moment(v).format("HH:mm:ss") : undefined) }} value={values.start_time ? new Date(moment(new Date()).format("yyyy-MM-DD") + " " + values?.start_time) : undefined} />
                        <Form.HelpText>{errors.start_time && errors.start_time}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>

            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="end_date">
                        <Form.ControlLabel>End date</Form.ControlLabel>
                        <DatePicker block name="end_date" format="yyyy-MM-dd" onChange={(v: Date | null) => setFieldValue("end_date", v ? moment(v).format("yyyy-MM-DD") : undefined)} value={values.end_date && new Date(values.end_date)} />
                        <Form.HelpText>{errors.end_date && errors.end_date}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="end_time">
                        <Form.ControlLabel>End time</Form.ControlLabel>
                        <DatePicker block name="end_time" format="HH:mm:ss" onChange={(v: Date | null) => setFieldValue("end_time", v ? moment(v).format("HH:mm:ss") : undefined)} value={values.end_time ? new Date(moment(new Date()).format("yyyy-MM-DD") + " " + values.end_time) : undefined} />
                        <Form.HelpText>{errors.end_time && errors.end_time}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>

            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form>
    )
}