import rest_api from 'app/rest-api';
import { WhatEvent, HowMuchEvent, WhenEvent, WhereEvent, StartSellingEvent } from 'models/event';

const EventApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createEvent: build.mutation({
            query: (payload: WhatEvent) => ({ method: "POST", url: '/event/events/', data: payload })
        }),
        updateEvent: build.mutation({
            query: (payload: WhatEvent) => ({ method: "PUT", url: '/event/events/' + payload.id + '/', data: payload })
        }),
        getAllEvent: build.mutation({
            query: () => ({ method: "GET", url: '/event/events/' })
        }),
        getEventById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/events/' + id + '/' })
        }),
        removeEventById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/events/' + id })
        }),
        getEventsByCategory: build.query({
            query: (category) => ({ method: "GET", url: '/event/events', params: { category: category } })
        }),


    })
})


export const {
    useCreateEventMutation,
    useUpdateEventMutation,
    useGetAllEventMutation,
    useGetEventByIdMutation,
    useRemoveEventByIdMutation,
    useGetEventsByCategoryQuery

} = EventApi

export default EventApi


export const HowMuchEventApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createHowMuchEvent: build.mutation({
            query: (payload: HowMuchEvent) => ({ method: "POST", url: '/event/how-much-event/', data: payload })
        }),
        updateHowMuchEvent: build.mutation({
            query: (payload: HowMuchEvent) => ({ method: "PUT", url: '/event/how-much-event/' + payload.id + '/', data: payload })
        }),
        getAllHowMuchEvent: build.mutation({
            query: () => ({ method: "GET", url: '/event/how-much-event/' })
        }),
        getHowMuchEventById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/how-much-event/' + id + '/' })
        }),
        removeHowMuchEventById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/how-much-event/' + id })
        }),
        getHowMuchEventsByEvent: build.mutation({
            query: (event) => ({ method: "GET", url: '/event/how-much-event', params: { event: event } })
        }),


    })
})


export const {
    useCreateHowMuchEventMutation,
    useUpdateHowMuchEventMutation,
    useGetAllHowMuchEventMutation,
    useGetHowMuchEventByIdMutation,
    useRemoveHowMuchEventByIdMutation,
    useGetHowMuchEventsByEventMutation

} = HowMuchEventApi


export const WhereEventApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createWhereEvent: build.mutation({
            query: (payload: WhereEvent) => ({ method: "POST", url: '/event/where-event/', data: payload })
        }),
        updateWhereEvent: build.mutation({
            query: (payload: WhereEvent) => ({ method: "PUT", url: '/event/where-event/' + payload.id + '/', data: payload })
        }),
        getAllWhereEvent: build.mutation({
            query: () => ({ method: "GET", url: '/event/where-event/' })
        }),
        getWhereEventById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/where-event/' + id + '/' })
        }),
        removeWhereEventById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/where-event/' + id })
        }),
        getWhereEventsByEvent: build.mutation({
            query: (event) => ({ method: "GET", url: '/event/where-event', params: { event: event } })
        }),


    })
})


export const {
    useCreateWhereEventMutation,
    useUpdateWhereEventMutation,
    useGetAllWhereEventMutation,
    useGetWhereEventByIdMutation,
    useRemoveWhereEventByIdMutation,
    useGetWhereEventsByEventMutation

} = WhereEventApi


export const WhenEventApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createWhenEvent: build.mutation({
            query: (payload: WhenEvent) => ({ method: "POST", url: '/event/when-event/', data: payload })
        }),
        updateWhenEvent: build.mutation({
            query: (payload: WhenEvent) => ({ method: "PUT", url: '/event/when-event/' + payload.id + '/', data: payload })
        }),
        getAllWhenEvent: build.mutation({
            query: () => ({ method: "GET", url: '/event/when-event/' })
        }),
        getWhenEventById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/when-event/' + id + '/' })
        }),
        removeWhenEventById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/when-event/' + id })
        }),
        getWhenEventsByEvent: build.mutation({
            query: (event) => ({ method: "GET", url: '/event/when-event', params: { event: event } })
        }),


    })
})


export const {
    useCreateWhenEventMutation,
    useUpdateWhenEventMutation,
    useGetAllWhenEventMutation,
    useGetWhenEventByIdMutation,
    useRemoveWhenEventByIdMutation,
    useGetWhenEventsByEventMutation

} = WhenEventApi


export const StartSellingEventApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createStartSellingEvent: build.mutation({
            query: (payload: StartSellingEvent) => ({ method: "POST", url: '/event/selling-event/', data: payload })
        }),
        updateStartSellingEvent: build.mutation({
            query: (payload: StartSellingEvent) => ({ method: "PUT", url: '/event/selling-event/' + payload.id + '/', data: payload })
        }),
        getAllStartSellingEvent: build.mutation({
            query: () => ({ method: "GET", url: '/event/selling-event/' })
        }),
        getStartSellingEventById: build.mutation({
            query: (id) => ({ method: "GET", url: '/event/selling-event/' + id + '/' })
        }),
        removeStartSellingEventById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/event/selling-event/' + id })
        }),
        getStartSellingEventsByEvent: build.mutation({
            query: (event) => ({ method: "GET", url: '/event/selling-event', params: { event: event } })
        }),


    })
})


export const {
    useCreateStartSellingEventMutation,
    useUpdateStartSellingEventMutation,
    useGetAllStartSellingEventMutation,
    useGetStartSellingEventByIdMutation,
    useRemoveStartSellingEventByIdMutation,
    useGetStartSellingEventsByEventMutation

} = StartSellingEventApi
