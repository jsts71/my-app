import { TicketTab, WhenEvent, WhenEventInitialValues } from 'models/event';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCreateWhenEventMutation, useGetWhenEventsByEventMutation, useUpdateWhenEventMutation } from '../redux/apis';

export interface UseWhenEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export interface UseWhenEventFormInterface {
    whenEventSchema: ObjectSchema<WhenEvent>
    initialValues: WhenEvent
    onSubmit: (values: WhenEvent) => void
}

export const useWhenEventForm = (props: UseWhenEventFormProps): UseWhenEventFormInterface => {

    const { id, setActive } = props

    const [getWhenEventsByEvent, { data: whereEventByEventData, isLoading: whereEventByEventIsLoading, isSuccess: whereEventByEventIsSuccess, isError: whereEventByEventIsError }] = useGetWhenEventsByEventMutation()
    const [createWhenEvent, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateWhenEventMutation()
    const [updateWhenEvent, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateWhenEventMutation()

    const [initialValues, setInitialValues] = useState<WhenEvent>(WhenEventInitialValues)

    useEffect(() => {
        if (id) {
            getWhenEventsByEvent(id).then((response: any) => {
                setInitialValues({ ...response.data.results[0], event: id })
            })
        }
    }, [id])

    useEffect(() => {
        createIsSuccess && setActive('How much')
        createIsError && toast.error("Something went wrong! Failed to create this event time!")
        createIsSuccess && toast.success("Event time created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    useEffect(() => {
        updateIsSuccess && setActive('How much')
        updateIsError && toast.error("Something went wrong! Failed to update this event time!")
        updateIsSuccess && toast.success("Event time updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])



    let whenEventSchema = object({
        id: number(),
        // timezone: number().required(),
        start_date: date().required(),
        end_date: date().required(),
        start_time: string().required(),
        end_time: string().required(),
        event: number(),
    });

    const onSubmit = (values: WhenEvent) => {
        console.log(values)
        values.id ?
            updateWhenEvent(values)
            :
            createWhenEvent(values)
    }

    return {
        whenEventSchema,
        initialValues,
        onSubmit
    }
}