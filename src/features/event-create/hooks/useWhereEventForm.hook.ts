import { TicketTab, WhereEvent, WhereEventInitialValues } from 'models/event';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCreateWhereEventMutation, useGetWhereEventsByEventMutation, useUpdateWhereEventMutation } from '../redux/apis';

export interface UseWhereEventFormProps {
    id: number
    setActive: (value: TicketTab) => void

}

export interface UseWhereEventFormInterface {
    whereEventSchema: ObjectSchema<WhereEvent>
    initialValues: WhereEvent
    onSubmit: (values: WhereEvent) => void
}

export const useWhereEventForm = (props: UseWhereEventFormProps): UseWhereEventFormInterface => {

    const { id, setActive } = props
    console.log('How much event id', id)

    const [getWhereEventsByEvent, { data: whereEventByEventData, isLoading: whereEventByEventIsLoading, isSuccess: whereEventByEventIsSuccess, isError: whereEventByEventIsError }] = useGetWhereEventsByEventMutation()
    const [createWhereEvent, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateWhereEventMutation()
    const [updateWhereEvent, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateWhereEventMutation()

    const [initialValues, setInitialValues] = useState<WhereEvent>(WhereEventInitialValues)

    useEffect(() => {
        if (id) {
            getWhereEventsByEvent(id).then((response: any) => {
                console.log(response)
                setInitialValues({ ...response.data.results[0], event: id })
            })
        }
    }, [id])

    useEffect(() => {
        createIsSuccess && setActive('When')
        createIsError && toast.error("Something went wrong! Failed to create this event location!")
        createIsSuccess && toast.success("Event location created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    useEffect(() => {
        updateIsSuccess && setActive('When')
        updateIsError && toast.error("Something went wrong! Failed to update this event location!")
        updateIsSuccess && toast.success("Event location updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])


    let whereEventSchema = object({
        id: number(),
        name: string().required(),
        description: string().required(),
        event: number().required(),
    });

    const onSubmit = (values: WhereEvent) => {
        console.log(values)
        values.id ?
            updateWhereEvent(values)
            :
            createWhereEvent(values)
    }
    
    return {
        whereEventSchema,
        initialValues,
        onSubmit
    }
}