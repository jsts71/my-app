import { StartSellingEvent, StartSellingEventInitialValues, TicketTab, WhatEvent } from 'models/event';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCreateStartSellingEventMutation, useGetEventByIdMutation, useGetStartSellingEventsByEventMutation, useUpdateEventMutation, useUpdateStartSellingEventMutation } from '../redux/apis';

export interface UseStartSellingEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export interface UseStartSellingEventFormInterface {
    startSellingEventSchema: ObjectSchema<StartSellingEvent>
    initialValues: StartSellingEvent
    onSubmit: (values: StartSellingEvent) => void
}

export const useStartSellingEventForm = (props: UseStartSellingEventFormProps): UseStartSellingEventFormInterface => {

    const { id, setActive } = props

    const [getStartSellingEventsByEvent, { data: whereEventByEventData, isLoading: whereEventByEventIsLoading, isSuccess: whereEventByEventIsSuccess, isError: whereEventByEventIsError }] = useGetStartSellingEventsByEventMutation()
    const [createStartSellingEvent, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateStartSellingEventMutation()
    const [updateStartSellingEvent, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateStartSellingEventMutation()
    const [updateEvent, { data: updateEventData, isLoading: updateEventIsLoading, isSuccess: updateEventIsSuccess, isError: updateEventIsError }] = useUpdateEventMutation()
    const [getEventById, { data: getData, isLoading: getIsLoading, isSuccess: getIsSuccess, isError: getIsError }] = useGetEventByIdMutation()

    const [initialValues, setInitialValues] = useState<StartSellingEvent>(StartSellingEventInitialValues)

    useEffect(() => {
        if (id) {
            getStartSellingEventsByEvent(id).then((response: any) => {
                response.data.results.length ?
                    setInitialValues({ ...response.data.results[0], event: id })
                    :
                    setInitialValues({ ...StartSellingEventInitialValues, event: id })
            })
        }
    }, [id])

    useEffect(() => {
        if (createIsSuccess) {
            getEventById(id).then((response: any) => {
                let data: WhatEvent = { ...response.data, id: id, status: 'Published', published: true }
                updateEvent(data).then(resp => {
                    setTimeout(()=>{
                        window.location.href = '/'
                    }, 5000)
                })
            })
        }
        createIsError && toast.error("Something went wrong! Failed to create this event selling!")
        createIsSuccess && toast.success("Event selling created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    useEffect(() => {
        if (updateIsSuccess) {
            getEventById(id).then((response: any) => {
                let data: WhatEvent = { ...response.data, id: id, status: 'Published', published: true }
                updateEvent(data).then(resp => {
                    setTimeout(()=>{
                        window.location.href = '/'
                    }, 5000)
                })
            })
        }
        updateIsError && toast.error("Something went wrong! Failed to update this event selling!")
        updateIsSuccess && toast.success("Event selling updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])


    let startSellingEventSchema: ObjectSchema<StartSellingEvent> = object({
        id: number(),
        refund: boolean().required(),
        policy: string(),
        terms_n_conditions: string(),
        stripe_payment: boolean().required(),
        event: number().required()
    });

    const onSubmit = (values: StartSellingEvent) => {
        console.log(values)
        values.id ?
            updateStartSellingEvent(values)
            :
            createStartSellingEvent(values)
    }

    return {
        startSellingEventSchema,
        initialValues,
        onSubmit
    }
}