import { useGetAllEventCategoryMutation } from 'features/event-category/redux/apis';
import { WhatEvent, EventCategory, TicketTab, EventDashboardTab, WhatEventInitialValues } from 'models/event';
import Yup, { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCallback, useEffect, useState } from 'react';
import _ from 'lodash';
import { ItemDataType } from 'rsuite/esm/@types/common';
import { useCreateEventMutation, useGetEventByIdMutation, useUpdateEventMutation } from '../redux/apis';
import { toast } from 'react-toastify';

export interface UseWhatEventFormProps {
    id: number
    setId: (value: number) => void
    setActive: (value: TicketTab) => void
}

export interface UseWhatEventFormInterface {
    whatEventSchema: ObjectSchema<WhatEvent>
    initialValues: WhatEvent
    onSubmit: (values: WhatEvent) => void
    categories: ItemDataType[]
    isLoading: boolean
}

export const useWhatEventForm = (props: UseWhatEventFormProps): UseWhatEventFormInterface => {

    const { id, setId, setActive } = props

    const [getAllEventCategory, { data, isLoading, isSuccess }] = useGetAllEventCategoryMutation()
    const [createEvent, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateEventMutation()
    const [updateEvent, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateEventMutation()
    const [getEventById, { data: getData, isLoading: getIsLoading, isSuccess: getIsSuccess, isError: getIsError }] = useGetEventByIdMutation()

    const [categories, setCategories] = useState<ItemDataType[]>([])
    const [initialValues, setInitialValues] = useState<WhatEvent>(WhatEventInitialValues)

    useEffect(() => {
        if (id) {
            getEventById(id).then((response: any) => {
                setInitialValues(response.data)
            })
        }
    }, [id])


    useEffect(() => {
        let pageLimit = parseInt(process.env.REACT_APP_PAGINATION_LIMIT ?? '10')
        let categoryPicker: ItemDataType[] = []

        const callbackFunction = async (page: number) => await getAllEventCategory('page=' + page).then((response: any) => {

            let responsePicker = _.map(response.data.results, (result: EventCategory) => { return { value: result.id, label: result.name } })
            categoryPicker = [...categoryPicker, ...responsePicker]
            setCategories(categoryPicker)

            if (response.data.count > pageLimit) {
                let totalPage: number = Math.ceil(response.data.count / pageLimit)
                if (page <= totalPage) {
                    page = page + 1
                    page <= totalPage && callbackFunction(page)
                }
            }
        })
        callbackFunction(1)
    }, [])

    useEffect(() => {
        createIsSuccess && setId(parseInt(createData.id))
        createIsSuccess && setActive('Where')
        createIsError && toast.error("Something went wrong! Failed to create this event!")
        createIsSuccess && toast.success("Event created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    useEffect(() => {
        updateIsSuccess && setActive('Where')
        updateIsError && toast.error("Something went wrong! Failed to update this event!")
        updateIsSuccess && toast.success("Event updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])

    let whatEventSchema: ObjectSchema<WhatEvent> = object({
        id: number(),
        title: string().required().max(200),
        description: string().required().max(2000),
        status: string().required(),
        category: number().required(),
        published: boolean().required(),
        // type: number().required(),
        // stream: number().required(),
        // stream_link: string().required()
    });


    const onSubmit = (values: WhatEvent) => {
        id ? updateEvent(values)
            :
            createEvent(values)
    }

    return {
        whatEventSchema,
        initialValues,
        onSubmit,
        categories,
        isLoading
    }
}