import { HowMuchEvent, HowMuchEventInitialValues, TicketTab } from 'models/event';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCreateHowMuchEventMutation, useGetHowMuchEventsByEventMutation, useUpdateHowMuchEventMutation } from '../redux/apis';

export interface UseHowMuchEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export interface UseHowMuchEventFormInterface {
    howMuchEventSchema: ObjectSchema<HowMuchEvent>
    initialValues: HowMuchEvent
    onSubmit: (values: HowMuchEvent) => void
}

export const useHowMuchEventForm = (props: UseHowMuchEventFormProps): UseHowMuchEventFormInterface => {

    const { id, setActive } = props
    console.log('How much event id', id)

    const [getHowMuchEventsByEvent, { data: howMuchEventByEventData, isLoading: howMuchEventByEventIsLoading, isSuccess: howMuchEventByEventIsSuccess, isError: howMuchEventByEventIsError }] = useGetHowMuchEventsByEventMutation()
    const [createHowMuchEvent, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateHowMuchEventMutation()
    const [updateHowMuchEvent, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateHowMuchEventMutation()

    const [initialValues, setInitialValues] = useState<HowMuchEvent>(HowMuchEventInitialValues)

    useEffect(() => {
        if (id) {
            getHowMuchEventsByEvent(id).then((response: any) => {
                console.log(response)
                setInitialValues({ ...response.data.results[0], event: id })
            })
        }
    }, [id])

    useEffect(() => {
        createIsSuccess && setActive('Start selling')
        createIsError && toast.error("Something went wrong! Failed to create this event price!")
        createIsSuccess && toast.success("Event price created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    useEffect(() => {
        updateIsSuccess && setActive('Start selling')
        updateIsError && toast.error("Something went wrong! Failed to update this event price!")
        updateIsSuccess && toast.success("Event price updated successfully.")
    }, [updateIsError, updateIsLoading, updateIsSuccess])


    let howMuchEventSchema = object({
        id: number(),
        quantity: number().required(),
        price: number().required(),
        event: number(),
    });

    const onSubmit = (values: HowMuchEvent) => {
        console.log(values)
        values.id ?
            updateHowMuchEvent(values)
            :
            createHowMuchEvent(values)
    }

    return {
        howMuchEventSchema,
        initialValues,
        onSubmit
    }
}