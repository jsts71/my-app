import { RS } from "components/regular";
import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from "formik";
import { TicketTab } from "models/event";
import { FC, useEffect } from "react";
import { Button, ButtonGroup, Col, Form, Input, Row, SelectPicker } from "rsuite";
import { useWhereEventForm } from "./hooks/useWhereEventForm.hook";

export interface WhereEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export const WhereEventForm: FC<WhereEventFormProps> = (props: WhereEventFormProps) => {

    const { id, setActive } = props

    const { whereEventSchema, initialValues, onSubmit } = useWhereEventForm({ id, setActive })
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: whereEventSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        setValues(initialValues)
    }, [initialValues])

    return (
        <Form fluid>
            <HeaderWithComponent h='Event details' s="8" c="green" />
            <Row className="show-grid">
                <Form.Group controlId="name">
                    <Form.ControlLabel>Address name<RS /></Form.ControlLabel>
                    <Form.Control name="name" onChange={(v: string) => setFieldValue("name", v)} value={values.name} />
                    <Form.HelpText>{errors.name && errors.name}</Form.HelpText>
                </Form.Group>
            </Row>

            <Row className="show-grid">
                <Form.Group controlId="description">
                    <Form.ControlLabel>Address description <RS /></Form.ControlLabel>
                    <Input as="textarea" rows={5} name="description" onChange={(v: string) => setFieldValue("description", v)} value={values?.description} />
                    <Form.HelpText>{errors.description && errors.description}</Form.HelpText>
                </Form.Group>
            </Row>

            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form>
    )
}