import { RS } from "components/regular";
import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from "formik";
import { TicketTab } from "models/event";
import { FC, useEffect, useState } from "react";
import { Button, ButtonGroup, Col, Form, Input, Row, SelectPicker } from "rsuite";
import { useWhatEventForm } from "./hooks/useWhatEventForm.hook";

export interface WhatEventFormProps {
    id: number
    setId: (value: number) => void
    setActive: (value: TicketTab) => void
}

export const WhatEventForm: FC<WhatEventFormProps> = (props: WhatEventFormProps) => {

    const { id, setId, setActive } = props

    const { whatEventSchema, initialValues, onSubmit, categories, isLoading } = useWhatEventForm({ setActive, id, setId })
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: whatEventSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        setValues(initialValues)
    }, [initialValues])

    return (
        <Form fluid>
            <HeaderWithComponent h='Event details' s="8" c="green" />
            <Row className="show-grid">
                <Form.Group controlId="title">
                    <Form.ControlLabel>Title<RS /></Form.ControlLabel>
                    <Form.Control name="title" onChange={(v: string) => setFieldValue("title", v)} value={values.title} />
                    <Form.HelpText>{errors.title && errors.title}</Form.HelpText>
                </Form.Group>
            </Row>
            <Row className="show-grid">
                <Form.Group controlId="category">
                    <Form.ControlLabel>Category<RS /></Form.ControlLabel>
                    <SelectPicker name="category" loading={isLoading} block data={categories} onChange={(v: number | string | null) => setFieldValue("category", v)} value={values.category} />
                    <Form.HelpText>{errors.category && errors.category}</Form.HelpText>
                </Form.Group>
            </Row>
            {/* <Row className="show-grid">
                <Form.Group controlId="type">
                    <Form.ControlLabel>Type</Form.ControlLabel>
                    <SelectPicker name="type" block data={data_num} onChange={(v: number | null) => setFieldValue("type", v)} value={values.type} />
                    <Form.HelpText>{errors.type && errors.type}</Form.HelpText>
                </Form.Group>
            </Row>
            <Row className="show-grid">
                <Form.Group controlId="stream">
                    <Form.ControlLabel>Stream</Form.ControlLabel>
                    <SelectPicker name="stream" block data={data_num} onChange={(v: number | null) => setFieldValue("stream", v)} value={values.stream} />
                    <Form.HelpText>{errors.stream && errors.stream}</Form.HelpText>
                </Form.Group>
            </Row>
            <Row className="show-grid">
                <Form.Group controlId="stream_link">
                    <Form.ControlLabel>Stream link</Form.ControlLabel>
                    <Form.Control name="stream_link" onChange={(v: string) => setFieldValue("stream_link", v)} value={values.stream_link} />
                    <Form.HelpText>{errors.stream_link && errors.stream_link}</Form.HelpText>
                </Form.Group>
            </Row> */}

            <Form.Group controlId="description">
                <Form.ControlLabel>Event description <RS /></Form.ControlLabel>
                <Input as="textarea" rows={5} name="description" onChange={(v: string) => setFieldValue("description", v)} value={values?.description} />
                <Form.HelpText>{errors.description && errors.description}</Form.HelpText>
            </Form.Group>

            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form>
    )
}