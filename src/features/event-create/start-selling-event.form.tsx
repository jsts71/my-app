import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from "formik";
import { TicketTab } from "models/event";
import { FC, useEffect, useState } from "react";
import { Button, ButtonGroup, Checkbox, Col, Form, IconButton, Input, Row, Toggle } from "rsuite";
import { useStartSellingEventForm } from "./hooks/useStartSellingEventForm.hook";
import { FaStripeS } from 'react-icons/fa';

import { Icon } from "@rsuite/icons";
import { DefaultDraftEditor } from "components/rich-text-editor/defaultDraftJsEditor";
import { convertFromRaw, convertToRaw, EditorState } from "draft-js";
import { RS } from "components/regular";

export interface StartSellingEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export const StartSellingEventForm: FC<StartSellingEventFormProps> = (props: StartSellingEventFormProps) => {

    const { id, setActive } = props

    const { startSellingEventSchema, initialValues, onSubmit } = useStartSellingEventForm({ id, setActive })
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: startSellingEventSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    const [policyState, setPolicyState] = useState(() => EditorState.createEmpty());
    const [terms_n_conditionsState, setTermsAndConditionsState] = useState(() => EditorState.createEmpty());

    useEffect(() => {
        setFieldValue("policy", JSON.stringify(convertToRaw(policyState.getCurrentContent())))
    }, [policyState])

    useEffect(() => {
        initialValues?.policy?.length && setPolicyState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.policy))))
    }, [initialValues?.policy])


    useEffect(() => {
        setFieldValue("terms_n_conditions", JSON.stringify(convertToRaw(terms_n_conditionsState.getCurrentContent())))
    }, [terms_n_conditionsState])

    useEffect(() => {
        initialValues?.terms_n_conditions?.length && setTermsAndConditionsState(EditorState.createWithContent(convertFromRaw(JSON.parse(initialValues?.terms_n_conditions))))
    }, [initialValues?.terms_n_conditions])


    useEffect(() => {
        setValues(initialValues)
    }, [initialValues])

    console.log(values)

    return (
        <Form fluid>
            <HeaderWithComponent h='Event terms and policies' s="8" c="green" />
            <Row className="show-grid">
                <Form.Group controlId="refund">
                    <Form.ControlLabel>Accept refund?</Form.ControlLabel>
                    <Toggle checkedChildren="Yes" unCheckedChildren="No" onChange={(v: boolean) => setFieldValue("refund", v)} checked={values.refund} />
                    <Form.HelpText>{errors.refund && errors.refund}</Form.HelpText>
                </Form.Group>
            </Row>
            <Row className="show-grid" hidden={!values.refund}>
                <Form.Group controlId="policy">
                    <Form.ControlLabel>Policy <RS /></Form.ControlLabel>
                    <DefaultDraftEditor editorState={policyState} setEditorState={(value: EditorState) => setPolicyState(value)} />
                    {/* <Input as="textarea" rows={5} name="ticket_template" onChange={(v: string) => setFieldValue("ticket_template", v)} value={values?.ticket_template} /> */}
                    <Form.HelpText>{errors.policy && errors.policy}</Form.HelpText>
                </Form.Group>
            </Row>

            <Row className="show-grid" hidden={!values.refund}>
                <Form.Group controlId="terms_n_conditions">
                    <Form.ControlLabel>Refund terms and conditions</Form.ControlLabel>
                    <DefaultDraftEditor editorState={terms_n_conditionsState} setEditorState={(value: EditorState) => setTermsAndConditionsState(value)} />
                    <Form.HelpText>{errors.terms_n_conditions && errors.terms_n_conditions}</Form.HelpText>
                </Form.Group>
            </Row >

            <HeaderWithComponent h='Activate credit card processor' s="8" c="green" />

            <Row className="show-grid">
                <Col xs={12}>
                    <IconButton block icon={<Icon as={FaStripeS} />} color="violet" appearance="primary" onClick={() => { setFieldValue('stripe_payment', !values.stripe_payment) }} disabled>Enable stripe</IconButton>
                </Col>
                <Col xs={12}>
                    <Checkbox readOnly checked={values.stripe_payment}> Stripe payment {values.stripe_payment ? 'enabled' : 'disabled'}</Checkbox>
                </Col>
            </Row>
            <hr />

            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Publish event</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form >
    )
}