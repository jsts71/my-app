import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from "formik";
import { TicketTab } from "models/event";
import { FC, useEffect } from "react";
import { Button, ButtonGroup, Col, Form, InputNumber, Row } from "rsuite";
import { useHowMuchEventForm } from "./hooks/useHowMuchEventForm.hook";

export interface HowMuchEventFormProps {
    id: number
    setActive: (value: TicketTab) => void
}

export const HowMuchEventForm: FC<HowMuchEventFormProps> = (props: HowMuchEventFormProps) => {

    const { id, setActive } = props

    const { howMuchEventSchema, initialValues, onSubmit } = useHowMuchEventForm({ id, setActive })
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: howMuchEventSchema });
    const { values, setFieldValue, handleSubmit, errors, setValues } = formik

    useEffect(() => {
        setValues(initialValues)
    }, [initialValues])

    return (
        <Form fluid>
            <HeaderWithComponent h='Ticket information' s="8" c="green" />
            {/* <Row className="show-grid">
                <Form.Group controlId="title">
                    <Form.ControlLabel>Ticket title</Form.ControlLabel>
                    <Form.Control name="title" onChange={(v: string) => setFieldValue("title", v)} value={values.title} />
                    <Form.HelpText>{errors.title && errors.title}</Form.HelpText>
                </Form.Group>
            </Row> */}
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="quantity">
                        <Form.ControlLabel>Quantity</Form.ControlLabel>
                        <InputNumber name="quantity" step={1} onChange={(v: any, e) => setFieldValue("quantity", v)} value={values.quantity} />
                        <Form.HelpText>{errors.quantity && errors.quantity}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="price">
                        <Form.ControlLabel>Price</Form.ControlLabel>
                        <InputNumber name="price" step={.1} onChange={(v: any, e) => setFieldValue("price", v)} value={values.price} />
                        <Form.HelpText>{errors.price && errors.price}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form>
    )
}