import { FC, useState } from "react";

import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import { FaStripeS } from "react-icons/fa";
import { IconButton } from "rsuite";
import { Icon } from "@rsuite/icons";
import { Stripe, StripeElements } from "@stripe/stripe-js";
import { toast } from "react-toastify";
import { PurchaseTicketTab, VolunteerTicket } from "models/ticket";

const CARD_OPTIONS = {
  // iconStyle: "solid",
  style: {
    base: {
      iconColor: "#531BA8",
      color: "#531BA8",
      fontWeight: 600,
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": { color: "#fce883" },
      "::placeholder": { color: "#87bbfd" },
    },
    invalid: {
      iconColor: "#ff1100",
      color: "#ff1100",
    },
  },
};

export interface PaymentFormCardElementProps {
  setActive: (value: PurchaseTicketTab) => void
  data: VolunteerTicket
}

export const PaymentFormCardElement: FC<PaymentFormCardElementProps> = (props: PaymentFormCardElementProps) => {
  const { setActive, data } = props;

  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let cardElement = elements?.getElement(CardElement);

    if (cardElement) {
      const result = await stripe?.createPaymentMethod({
        type: "card",
        card: cardElement,
      });

      if (result) {
        const { error, paymentMethod } = result;
        if (!error) {
          try {
            const { id } = paymentMethod;

            // __________________________________________________________________________
            // Ticket will be saved with userId, volunteerTicketId, stripePaymentId
            // const ab = function.then(isError then ab(), isSuccess then do as succes)
            // QR code will return stripe payment is as string only
            // In event admin will check qr and find the payment id and mark id as attendent
            // Also download the ticket
            // there will be a ticket list, create for if anytime server faild to create ticket, no delete, no update
            // __________________________________________________________________________

            // const response = await axios.post("http://localhost:4000/payment", {
            //     amount: 1000,
            //     id
            // })

            // if (response.data.success) {
            //     toast.success("Successful payment "+ id)
            // }
          } catch (error) {
            console.log("Error", error);
            toast.error(error as string);
          }
        } else {
          toast.error(error.message);
          setActive("Ticket");
        }
      }
    }
  };

  return (
    <>
      <div className="card-element">
        <CardElement options={CARD_OPTIONS} />
      </div>
      <IconButton
        block
        icon={<Icon as={FaStripeS} />}
        color="violet"
        appearance="primary"
        onClick={handleSubmit}
      >
        Submit
      </IconButton>
    </>
  );
};
