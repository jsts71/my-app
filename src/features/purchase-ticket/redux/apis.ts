import rest_api from 'app/rest-api';
import { StripePayment } from 'models/payment';

const StripePaymentApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createStripePayment: build.mutation({
            query: (payload: StripePayment) => ({ method: "POST", url: '/payment/volunteer-stripe-payment/', data: payload })
        }),
        updateStripePayment: build.mutation({
            query: (payload: StripePayment) => ({ method: "PUT", url: '/payment/volunteer-stripe-payment/' + payload.id + '/', data: payload })
        }),
        getAllStripePayment: build.mutation({
            query: () => ({ method: "GET", url: '/payment/volunteer-stripe-payment/' })
        }),
        getStripePaymentById: build.mutation({
            query: (id) => ({ method: "GET", url: '/payment/volunteer-stripe-payment/' + id + '/' })
        }),
        removeStripePaymentById: build.mutation({
            query: (id) => ({ method: "DELETE", url: '/payment/volunteer-stripe-payment/' + id })
        }),
        getStripePaymentsByCategory: build.query({
            query: (category) => ({ method: "GET", url: '/payment/volunteer-stripe-payment', params: { category: category } })
        }),
    })
})


export const {
    useCreateStripePaymentMutation,
    useUpdateStripePaymentMutation,
    useGetAllStripePaymentMutation,
    useGetStripePaymentByIdMutation,
    useRemoveStripePaymentByIdMutation,
    useGetStripePaymentsByCategoryQuery

} = StripePaymentApi

export default StripePaymentApi
