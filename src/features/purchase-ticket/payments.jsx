import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { PaymentForm } from "./paymentForm";
import { DefaultBand } from "components/band/defaultBand";
import { useCreateStripePaymentMutation } from "./redux/apis";
import "./style.scss";

export const Payment = (props) => {
  const { setActive, data } = props;
  const { id, eventId } = useParams();
  const [clientSecret, setClientSecret] = useState("");
  const [respData, setRespData] = useState();

  const stripePromise = loadStripe(
    "pk_test_51MlwFsKd9VSAKprDnhKn4lGQVZCBrJhgtjnLajYk8mecEc4HTi0uwB7pbT5jpTSnc0zjfqINPMXgQD1qOWzNuA6E002zA3GxBa"
  );
  const secretKey =
    "sk_test_51MlwFsKd9VSAKprDW15hfl4ubu7gQFRazLS00VFujZP3SyBoJhAGGxm9sFe5ewangXNnoIM5ClbtRl2QTXqDALPW00LazmKKPp";

  console.log(respData);

  const [
    createStripePayment,
    {
      data: createData,
      isLoading: createIsLoading,
      isSuccess: createIsSuccess,
      isError: createIsError,
    },
  ] = useCreateStripePaymentMutation();

  let body = {
    email: "sample@gmail.com",
    currency: "aud",
    volunteer_ticket: parseInt(id),
    secret_key: secretKey,
    purchased: false,
  };

  useEffect(() => {
    // Create PaymentIntent as soon as the page loads
    createStripePayment(body)
      .then((response) => {
        setRespData(response.data);
        console.log(
          "-----------------------------",
          response,
          response.data.clientSecret
        );
        setClientSecret(response.data.clientSecret);
      })
      .then((data) => {
        setRespData(data.amount);
        console.log(data.amount);
        setClientSecret(data.clientSecret);
      });
  }, []);

  const appearance = {
    theme: "stripe",
  };

  const options = {
    clientSecret,
    //   "pi_3MmjVUKd9VSAKprD1mqQpLYT_secret_xxmiRRyICoQ7IkgWlHfAcSdeL",
    appearance,
  };

  return (
    <>
      <DefaultBand
        text="Stripe payment"
        subtext="Stripe is a suite of APIs powering online payment processing and commerce solutions for internet businesses of all sizes. Accept payments and scale faster."
      />
      {clientSecret && (
        <Elements options={options} stripe={stripePromise}>
          <PaymentForm
            {...props}
            respData={respData}
            clientSecret={clientSecret}
          />
        </Elements>
      )}
    </>
  );
};
