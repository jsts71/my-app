import { DefaultDraftView } from 'components/rich-text-editor/defaultDraftJsView'
import { useGetVolunteerTicketByIdMutation } from "features/volunteer-ticket/redux/apis";
import { convertFromRaw, EditorState } from 'draft-js';
import { FC, useState } from 'react';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import QRCode from "react-qr-code";
import "./style.scss"
import { PurchaseTicketTab, VolunteerTicket } from 'models/ticket';
import { Button, ButtonGroup, Checkbox } from 'rsuite';

export interface AggreementProps {
    setActive: (value: PurchaseTicketTab) => void
    data: VolunteerTicket
}

export const Aggreement: FC<AggreementProps> = (props: AggreementProps) => {

    const { setActive, data } = props
    const { id, eventId } = useParams()
    const [agree, setAgree] = useState(false)

    let img_link = 'https://png.pngtree.com/background/20210714/original/pngtree-blue-carbon-background-with-sport-style-and-golden-light-picture-image_1200848.jpg'

    return (
        <>
            <img src={img_link} />
            <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['agreement_template'])))} border={false} />
            <Checkbox onChange={() => setAgree(!agree)} checked={agree}> I agree . . .</Checkbox>
            <ButtonGroup justified block>
                <Button disabled={agree} onClick={() => setActive('Introduction')} appearance="ghost">Back</Button>
                <Button disabled={!agree} onClick={() => setActive('Whs')} appearance="ghost">Next</Button>
            </ButtonGroup>
        </>
    )
}