import { StripePayment, StripePaymentInitialValues, StripePaymentValidationSchema } from 'models/payment';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
import { useCreateStripePaymentMutation, useUpdateStripePaymentMutation } from '../redux/apis';

export interface UseStripePaymentFormProps {
    // id: number
    // setActive: (value: TicketTab) => void
}

export interface UseStripePaymentFormInterface {
    stripePaymentSchema: ObjectSchema<StripePayment>
    initialValues: StripePayment
    onSubmit: (values: StripePayment) => void
}

export const useStripePaymentForm = (props: UseStripePaymentFormProps): UseStripePaymentFormInterface => {

    // const { id, setActive } = props
    // console.log('How much event id', id)

    // const [getStripePaymentsByEvent, { data: stripePaymentByEventData, isLoading: stripePaymentByEventIsLoading, isSuccess: stripePaymentByEventIsSuccess, isError: stripePaymentByEventIsError }] = useGetStripePaymentsByEventMutation()
    const [createStripePayment, { data: createData, isLoading: createIsLoading, isSuccess: createIsSuccess, isError: createIsError }] = useCreateStripePaymentMutation()
    // const [updateStripePayment, { data: updateData, isLoading: updateIsLoading, isSuccess: updateIsSuccess, isError: updateIsError }] = useUpdateStripePaymentMutation()

    const [initialValues, setInitialValues] = useState<StripePayment>(StripePaymentInitialValues)
    const [stripePaymentSchema, setStripePaymentSchema] = useState<ObjectSchema<StripePayment>>(StripePaymentValidationSchema)

    // useEffect(() => {
    //     if (id) {
    //         getStripePaymentsByEvent(id).then((response: any) => {
    //             console.log(response)
    //             setInitialValues({ ...response.data.results[0], event: id })
    //         })
    //     }
    // }, [id])

    useEffect(() => {
        // createIsSuccess && setActive('Start selling')
        createIsError && toast.error("Something went wrong! Failed to create this event price!")
        createIsSuccess && toast.success("Event price created successfully.")
    }, [createIsError, createIsLoading, createIsSuccess])

    // useEffect(() => {
    //     updateIsSuccess && setActive('Start selling')
    //     updateIsError && toast.error("Something went wrong! Failed to update this event price!")
    //     updateIsSuccess && toast.success("Event price updated successfully.")
    // }, [updateIsError, updateIsLoading, updateIsSuccess])


    // let stripePaymentSchema = object({
    //     id: number(),
    //     quantity: number().required(),
    //     price: number().required(),
    //     event: number(),
    // });

    const onSubmit = (values: StripePayment) => {
        // console.log(values)
        // values.id ?
        //     updateStripePayment(values)
        //     :
        createStripePayment(values)
    }

    return {
        stripePaymentSchema,
        initialValues,
        onSubmit
    }
}