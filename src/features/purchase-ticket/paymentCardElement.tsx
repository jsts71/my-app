
import { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { PaymentForm } from './paymentForm';
import "./style.scss"
import { DefaultBand } from 'components/band/defaultBand';
import { PurchaseTicketTab, VolunteerTicket } from 'models/ticket';


export interface PaymentCardElementProps {
    setActive: (value: PurchaseTicketTab) => void
    data: VolunteerTicket
}

export const PaymentCardElement: FC<PaymentCardElementProps> = (props: PaymentCardElementProps) => {

    const { setActive, data } = props
    const { id, eventId } = useParams()

    const stripePromise = loadStripe('pk_test_51MlwFsKd9VSAKprDnhKn4lGQVZCBrJhgtjnLajYk8mecEc4HTi0uwB7pbT5jpTSnc0zjfqINPMXgQD1qOWzNuA6E002zA3GxBa');
    

    return (
        <>
            <DefaultBand text="Stripe payment" subtext="Stripe is a suite of APIs powering online payment processing and commerce solutions for internet businesses of all sizes. Accept payments and scale faster." />
            <Elements stripe={stripePromise}>
                <PaymentForm {...props} />
            </Elements>
        </>
    )
}