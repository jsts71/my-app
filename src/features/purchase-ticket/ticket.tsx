import { DefaultDraftView } from 'components/rich-text-editor/defaultDraftJsView'
import { useGetVolunteerTicketByIdMutation } from "features/volunteer-ticket/redux/apis";
import { convertFromRaw, EditorState } from 'draft-js';
import { FC } from 'react';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import QRCode from "react-qr-code";
import "./style.scss"
import { PurchaseTicketTab, VolunteerTicket } from 'models/ticket';

export interface TicketProps {
    setActive: (value: PurchaseTicketTab) => void
    data: VolunteerTicket
}

export const Ticket: FC<TicketProps> = (props: TicketProps) => {

    const { id, eventId } = useParams()

    let img_link = 'https://png.pngtree.com/background/20210714/original/pngtree-blue-carbon-background-with-sport-style-and-golden-light-picture-image_1200848.jpg'

    const [getVolunteerTicketById, { data, isLoading, isSuccess }] = useGetVolunteerTicketByIdMutation()

    useEffect(() => {
        id && getVolunteerTicketById(id)
    }, [id])

    console.log(window.location.href, data, id, eventId)

    return (
        <>
            Here a PDF will be generated !<br/><br/><br/>
            <div className='html-document'>
                <img src={img_link} />
                <div className='details-border'>
                    {isSuccess && <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['ticket_template'])))} color="black" border={false} />}
                </div>
                <div className='qr-code'>
                    <QRCode
                        size={256}
                        style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                        value={window.location.href}
                        viewBox={`0 0 256 256`}
                    />
                </div>
                <div className='indentity'>
                    <p><b>Name</b>: {'No buyer'}</p>
                    <p><b>Purchase</b> : {'Purchase id'}</p>
                </div>
                <h5 style={{ color: 'green' }}>Note</h5>
                <div className='details-no-border'>
                    {isSuccess && <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['introduction_template'])))} color="green" border={false} />}
                </div>
                <h5 style={{ color: 'red' }}>Terms and conditions</h5>
                <div className='details-no-border'>
                    {isSuccess && <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['agreement_template'])))} color="red" border={false} />}
                </div>


            </div>
        </>
    )
}