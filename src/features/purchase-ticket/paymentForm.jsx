import { useState, useEffect } from "react";

import {
  LinkAuthenticationElement,
  PaymentElement,
  useElements,
  useStripe,
} from "@stripe/react-stripe-js";
import { FaStripeS } from "react-icons/fa";
import { Col, Form, IconButton, Row } from "rsuite";
import { Icon } from "@rsuite/icons";
import { toast } from "react-toastify";
import { useUpdateStripePaymentMutation } from "./redux/apis";

export const PaymentForm = (props) => {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const { setActive, data, respData, clientSecret } = props;

  const stripe = useStripe();
  const elements = useElements();

  const [
    updateStripePayment,
    {
      data: updateData,
      isLoading: updateIsLoading,
      isSuccess: updateIsSuccess,
      isError: updateIsError,
    },
  ] = useUpdateStripePaymentMutation();

  useEffect(() => {
    if (!stripe) return;
    if (!clientSecret) return;

    stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
      switch (paymentIntent.status) {
        case "succeeded":
          setMessage("Payment succeeded!");
          toast.success("Payment succeeded!");
          break;
        case "processing":
          setMessage("Your payment is processing.");
          updateStripePayment({
            ...respData.data,
            email: email,
            purchased: true,
          });
          toast.info("Your payment is processing.");
          break;
        case "requires_payment_method":
          setMessage("Your payment was not successful, please try again.");
          toast.error("Your payment was not successful, please try again.");
          break;
        default:
          setMessage("Something went wrong.");
          toast.error("Something went wrong.");
          break;
      }
    });
  }, [stripe]);

  const handleSubmit = async (e) => {
    updateStripePayment({ ...respData.data, email: email, purchased: true });
    e.preventDefault();
    // toast.info('Your request is in process!')
    if (!stripe || !elements) {
      return;
    }
    setIsLoading(true);
    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        // Make sure to change this to your payment completion page
        return_url: "http://localhost:3000",
      },
    });

    // This point will only be reached if there is an immediate error when
    // confirming the payment. Otherwise, your customer will be redirected to
    // your `return_url`. For some payment methods like iDEAL, your customer will
    // be redirected to an intermediate site first to authorize the payment, then
    // redirected to the `return_url`.

    if (error.type === "card_error" || error.type === "validation_error") {
      setMessage(error.message);
      toast.error(error.message);
    } else {
      setMessage("An unexpected error occurred.");
      toast.error("An unexpected error occurred.");
    }
    setIsLoading(false);
  };

  const paymentElementOptions = {
    layout: "tabs",
  };

  return (
    <>
      <div className="payment-element">
        <Row
          className="show-grid"
          style={{
            padding: "30px 30px 10px 30px",
            color: "black",
            fontSize: "20px",
          }}
        >
          <Col xs={12}>Amount : {parseInt(respData?.amount) / 100}</Col>
          <Col xs={12}>Currency : AUD</Col>
        </Row>
        <LinkAuthenticationElement
          id="link-authentication-element"
          onChange={(e) => setEmail(e.value.email)}
        />
        <PaymentElement id="payment-element" options={paymentElementOptions} />
      </div>
      <IconButton
        block
        icon={<Icon as={FaStripeS} />}
        color="violet"
        appearance="primary"
        onClick={handleSubmit}
      >
        Submit
      </IconButton>
    </>
  );
};
