import { DefaultDraftView } from 'components/rich-text-editor/defaultDraftJsView'
import { useGetVolunteerTicketByIdMutation } from "features/volunteer-ticket/redux/apis";
import { convertFromRaw, EditorState } from 'draft-js';
import { FC, useState } from 'react';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import QRCode from "react-qr-code";
import "./style.scss"
import { PurchaseTicketTab, VolunteerTicket } from 'models/ticket';
import { Button, ButtonGroup, Checkbox } from 'rsuite';

export interface TemplateProps {
    setActive: (value: PurchaseTicketTab) => void
    data: VolunteerTicket
}

export const Template: FC<TemplateProps> = (props: TemplateProps) => {

    const { setActive, data } = props
    const { id, eventId } = useParams()
    const [agree, setAgree] = useState(false)

    let img_link = 'https://png.pngtree.com/background/20210714/original/pngtree-blue-carbon-background-with-sport-style-and-golden-light-picture-image_1200848.jpg'

    return (
        <>
            <div className='html-document'>
                <img src={img_link} />
                <div className='details-border'>
                    <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['ticket_template'])))} color="black" border={false} />
                </div>
                <div className='qr-code'>
                    <QRCode
                        size={256}
                        style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                        value={window.location.href}
                        viewBox={`0 0 256 256`}
                    />
                </div>
                <div className='indentity'>
                    <p><b>Name</b>: {'No buyer'}</p>
                    <p><b>Purchase</b> : {'Purchase id'}</p>
                </div>
                <h5 style={{ color: '#d9a600' }}>Note</h5>
                <div className='details-no-border'>
                    <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['note'])))} color="#d9a600" border={false} />
                </div>
                <h5 style={{ color: 'red' }}>Terms and conditions</h5>
                <div className='details-no-border'>
                    <DefaultDraftView editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data['terms_n_condition'])))} color="red" border={false} />
                </div>
            </div>
            <Checkbox onChange={() => setAgree(!agree)} checked={agree}> I agree . . .</Checkbox>
            <ButtonGroup justified block>
                <Button disabled={agree} onClick={() => setActive('Whs')} appearance="ghost">Back</Button>
                <Button disabled={!agree} onClick={() => setActive('Payment')} appearance="ghost">Next</Button>
            </ButtonGroup>
        </>
    )
}