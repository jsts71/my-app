import { FC } from "react";

import "./style.scss"

export interface ConstFooterProps {

}

export const ConstFooter: FC<ConstFooterProps> = (props: ConstFooterProps) => {
    const { } = props;

    return (
        <div className="const-footer">Admin Authorired &#8373; <b>Abir Hosen Ashik</b> - {(new Date()).getFullYear()}</div>

    )
}