import rest_api from 'app/rest-api';
import { VolunteerTicket } from 'models/ticket';
import { VolunteerApplication} from 'models/volunteer';

const volunteerApi = rest_api.injectEndpoints({
    overrideExisting: true,
    endpoints: (build) => ({
        createVolunteer: build.mutation({
            query: (payload: VolunteerApplication) => ({ method: "POST", url: '/volunteer', data: payload })
        }),
        updateVolunteer: build.mutation({
            query: (payload: VolunteerApplication) => ({ method: "PUT", url: '/volunteer', data: payload })
        }),
        getAllVolunteers: build.query({
            query: () => ({ method: "GET", url: '/volunteer' })
        }),
        getVolunteerById: build.query({
            query: (id) => ({ method: "GET", url: '/volunteer', params: { id: id } })
        }),
        getVolunteersByEvent: build.query({
            query: (id) => ({ method: "GET", url: '/volunteer', params: { event_id: id } })
        }),

        createVolunteerTicket: build.mutation({
            query: (payload: VolunteerTicket) => ({ method: "POST", url: "/volunteer/ticket", data: payload })
        })


    })
})


export const {
    useCreateVolunteerMutation,

    useCreateVolunteerTicketMutation
} = volunteerApi
export default volunteerApi