import { VolunteerApplication } from 'models/volunteer';
import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';

export interface VolunteerApplicationFormHookProps {

}

export interface VolunteerApplicationFormHookInterface {
    volunteerSchema: ObjectSchema<VolunteerApplication>
    initialValues: VolunteerApplication
    onSubmit: (values: VolunteerApplication) => void
}

export const useVolunteerApplicationFormHook = (props: VolunteerApplicationFormHookProps): VolunteerApplicationFormHookInterface => {

    let volunteerSchema = object({
        first_name: string().required('First name is a required field.'),
        last_name: string().required('Last name is a required field.'),
        prefered_name: string().required('Prefered name is a required field.'),
        email: string().email('Enter a valid email').required('Email is a required field.'),
        confirm_email: string().required('Confirm email is a required field.').oneOf([ref('email')], 'Email must match.'),
        phone: string().required(),
        zip: string(),
        city: string().required(),
        state: string().required(),
        country: string().required(),
        fb_link: string().required(),
        dob: date(),
        experience: string().required(),
        experience_description: string(),
        current_job: string().required(),
        qualifications: array().of(string().required()).required(),
        about: string(),
        comments: string(),
        preferred_tasks: array().of(string().required()).required().min(1),
        preferred_tasks_info: string(),
        preferred_time_frame: array().of(string().required()).required().min(1),
        volunteering_with_friends: array().of(string().required()),
        volunteering_with_head: boolean(),
        dept_head_name: string(),
        emg_first_name: string().required(),
        emg_last_name: string(),
        emg_relation_name: string().required(),
        emg_phone: string().required(),
    });

    let initialValues = {
        first_name: '',
        last_name: '',
        prefered_name: '',
        email: '',
        confirm_email: '',
        phone: '',
        zip: '',
        city: '',
        state: '',
        country: '',
        fb_link: '',
        dob: undefined,
        experience: '',
        experience_description: '',
        current_job: '',
        qualifications: [],
        about: '',
        comments: '',
        preferred_tasks: [],
        preferred_tasks_info: '',
        preferred_time_frame: [],
        volunteering_with_friends: [],
        volunteering_with_head: false,
        dept_head_name: '',
        emg_first_name: '',
        emg_last_name: '',
        emg_relation_name: '',
        emg_phone: '',
    }

    const onSubmit = (values: VolunteerApplication) => {
        console.log(values)
    }

    return {
        volunteerSchema,
        initialValues,
        onSubmit
    }
}