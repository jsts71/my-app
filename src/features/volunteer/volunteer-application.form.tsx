import { FC } from "react";
import { Button, ButtonGroup, ButtonToolbar, CheckPicker, Col, DatePicker, Form, Input, Row, SelectPicker, Toggle } from "rsuite";
import { useVolunteerApplicationFormHook } from "./hooks/useVolunteerApplicationForm.hook";
import HeaderWithComponent from "features/heading/header-with-components";
import { useFormik } from 'formik';

import "./style.scss"
import { RS } from "components/regular";
import { useCreateVolunteerMutation } from "./redux/apis";

export interface VolunteerApplicationFormProps {

}

export const VolunteerApplicationForm: FC<VolunteerApplicationFormProps> = (props: VolunteerApplicationFormProps) => {

    const { volunteerSchema, initialValues, onSubmit } = useVolunteerApplicationFormHook({})
    const formik = useFormik({ initialValues: initialValues, onSubmit, validateOnChange: false, validationSchema: volunteerSchema });
    const { values, setFieldValue, handleSubmit, errors } = formik
    const data = ['Eugenia', 'Bryan', 'Linda', 'Nancy', 'Lloyd', 'Alice', 'Julia', 'Albert'].map(
        item => ({ label: item, value: item })
    );
    const {} = useCreateVolunteerMutation()

    const range = [new Date(), new Date()]

    return (
        <Form fluid>
            <HeaderWithComponent h='Basic information' s="8" c="green" />
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="first_name">
                        <Form.ControlLabel>First Name <RS /></Form.ControlLabel>
                        <Form.Control name="first_name" onChange={(v: string) => setFieldValue("first_name", v)} value={values.first_name} />
                        <Form.HelpText>{errors.first_name && errors.first_name}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="last_name">
                        <Form.ControlLabel>Last Name <RS /></Form.ControlLabel>
                        <Form.Control name="last_name" onChange={(v: string) => setFieldValue("last_name", v)} />
                        <Form.HelpText>{errors.last_name && errors.last_name}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group controlId="prefered_name">
                <Form.ControlLabel>Preferred Name <RS /></Form.ControlLabel>
                <Form.Control name="prefered_name" onChange={(v: string) => setFieldValue("prefered_name", v)} />
                <Form.HelpText>{errors.prefered_name && errors.prefered_name}</Form.HelpText>
            </Form.Group>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="email">
                        <Form.ControlLabel>Email <RS /></Form.ControlLabel>
                        <Form.Control name="email" type="email" onChange={(v: string) => setFieldValue("email", v)} />
                        <Form.HelpText>{errors.email && errors.email}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="confirm_email">
                        <Form.ControlLabel>Confirm email <RS /></Form.ControlLabel>
                        <Form.Control name="confirm_email" type="email" onChange={(v: string) => setFieldValue("confirm_email", v)} />
                        <Form.HelpText>{errors.confirm_email && errors.confirm_email}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="phone">
                        <Form.ControlLabel>Phone <RS /></Form.ControlLabel>
                        <Form.Control name="phone" onChange={(v: string) => setFieldValue("phone", v)} />
                        <Form.HelpText>{errors.phone && errors.phone}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="zip">
                        <Form.ControlLabel>Postal code</Form.ControlLabel>
                        <Form.Control name="zip" onChange={(v: string) => setFieldValue("zip", v)} />
                        <Form.HelpText>{errors.zip && errors.zip}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <HeaderWithComponent h='About you' s="8" c="green" />
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="city">
                        <Form.ControlLabel>City <RS /></Form.ControlLabel>
                        <Form.Control name="city" onChange={(v: string) => setFieldValue("city", v)} />
                        <Form.HelpText>{errors.city && errors.city}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="state">
                        <Form.ControlLabel>State <RS /></Form.ControlLabel>
                        <Form.Control name="state" onChange={(v: string) => setFieldValue("state", v)} />
                        <Form.HelpText>{errors.state && errors.state}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="country">
                        <Form.ControlLabel>Country <RS /></Form.ControlLabel>
                        <Form.Control name="country" onChange={(v: string) => setFieldValue("country", v)} />
                        <Form.HelpText>{errors.country && errors.country}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="fb_link">
                        <Form.ControlLabel>Facebook profile link <RS /></Form.ControlLabel>
                        <Form.Control name="fb_link" onChange={(v: string) => setFieldValue("fb_link", v)} />
                        <Form.HelpText>{errors.fb_link && errors.fb_link}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Row className="show-grid">
                        <Form.Group controlId="dob">
                            <Form.ControlLabel>Date of birth <RS /></Form.ControlLabel>
                            <DatePicker block name="dob" onChange={(v: Date | null) => setFieldValue("dob", v)} />
                            <Form.HelpText>{errors.dob && errors.dob}</Form.HelpText>
                        </Form.Group>
                    </Row>
                    <Row className="show-grid">
                        <Form.Group controlId="current_job">
                            <Form.ControlLabel>Current job / position <RS /></Form.ControlLabel>
                            <Form.Control name="current_job" onChange={(v: string) => setFieldValue("current_job", v)} />
                            <Form.HelpText>{errors.current_job && errors.current_job}</Form.HelpText>
                        </Form.Group>
                    </Row>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="about">
                        <Form.ControlLabel>About yourself</Form.ControlLabel>
                        <Input as="textarea" rows={5} name="about" onChange={(v: string) => setFieldValue("about", v)} />
                        <Form.HelpText>{errors.about && errors.about}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <HeaderWithComponent h='About your job' s="8" c="green" />
            <Row className="show-grid">
                <Col xs={12}>
                    <Row className="show-grid">
                        <Form.Group controlId="qualifications">
                            <Form.ControlLabel>Qualification <RS /></Form.ControlLabel>
                            <CheckPicker data={data} block name="qualifications" onChange={(v: string[]) => setFieldValue("qualifications", v)} />
                            <Form.HelpText>{errors.qualifications && errors.qualifications}</Form.HelpText>
                        </Form.Group>
                    </Row>
                    <Row className="show-grid">
                        <Form.Group controlId="experience">
                            <Form.ControlLabel>Experience <RS /></Form.ControlLabel>
                            <Form.Control name="experience" onChange={(v: string) => setFieldValue("experience", v)} />
                            <Form.HelpText>{errors.experience && errors.experience}</Form.HelpText>
                        </Form.Group>
                    </Row>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="experience_description">
                        <Form.ControlLabel>Experience description</Form.ControlLabel>
                        <Input as="textarea" rows={5} name="experience_description" onChange={(v: string) => setFieldValue("experience_description", v)} />
                        <Form.HelpText>{errors.experience_description && errors.experience_description}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Row className="show-grid">
                        <Form.Group controlId="preferred_tasks">
                            <Form.ControlLabel>Preferred tasks <RS /></Form.ControlLabel>
                            <CheckPicker data={data} block name="preferred_tasks" onChange={(v: string[]) => setFieldValue("preferred_tasks", v)} />
                            <Form.HelpText>{errors.preferred_tasks && errors.preferred_tasks}</Form.HelpText>
                        </Form.Group>
                    </Row>
                    <Row className="show-grid">
                        <Form.Group controlId="preferred_time_frame">
                            <Form.ControlLabel>Preferred time frame <RS /></Form.ControlLabel>
                            <CheckPicker block name="preferred_time_frame" data={data} onChange={(v: string[]) => setFieldValue("preferred_time_frame", v)} />
                            <Form.HelpText>{errors.preferred_time_frame && errors.preferred_time_frame}</Form.HelpText>
                        </Form.Group>

                    </Row>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="preferred_tasks_info">
                        <Form.ControlLabel>Preferred tasks info</Form.ControlLabel>
                        <Input as="textarea" rows={5} name="preferred_tasks_info" onChange={(v: string) => setFieldValue("preferred_tasks_info", v)} />
                        <Form.HelpText>{errors.preferred_tasks_info && errors.preferred_tasks_info}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="volunteering_with_friends">
                        <Form.ControlLabel>Volunteering with friends</Form.ControlLabel>
                        <CheckPicker block name="volunteering_with_friends" data={data} onChange={(v: string[]) => setFieldValue("volunteering_with_friends", v)} />
                        <Form.HelpText>{errors.volunteering_with_friends && errors.volunteering_with_friends}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={4}>
                    <Form.Group controlId="volunteering_with_head">
                        <Form.ControlLabel>Volunteering with head?</Form.ControlLabel>
                        <Toggle checkedChildren="Yes" unCheckedChildren="No" onChange={(v: boolean) => setFieldValue("volunteering_with_head", v)} />
                        <Form.HelpText>{errors.volunteering_with_head && errors.volunteering_with_head}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={8}>
                    <Form.Group controlId="dept_head_name">
                        <Form.ControlLabel>Dept. head name</Form.ControlLabel>
                        <Form.Control name="dept_head_name" onChange={(v: string) => setFieldValue("dept_head_name", v)} />
                        <Form.HelpText>{errors.dept_head_name && errors.dept_head_name}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <HeaderWithComponent h='Emergency contacts' s="8" c="green" />
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="emg_first_name">
                        <Form.ControlLabel>First Name <RS /></Form.ControlLabel>
                        <Form.Control name="emg_first_name" onChange={(v: string) => setFieldValue("emg_first_name", v)} value={values.emg_first_name} />
                        <Form.HelpText>{errors.emg_first_name && errors.emg_first_name}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="emg_last_name">
                        <Form.ControlLabel>Last Name</Form.ControlLabel>
                        <Form.Control name="emg_last_name" onChange={(v: string) => setFieldValue("emg_last_name", v)} />
                        <Form.HelpText>{errors.emg_last_name && errors.emg_last_name}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="show-grid">
                <Col xs={12}>
                    <Form.Group controlId="emg_relation_name">
                        <Form.ControlLabel>Relationship Name <RS /></Form.ControlLabel>
                        <Form.Control name="emg_relation_name" onChange={(v: string) => setFieldValue("emg_relation_name", v)} value={values.emg_relation_name} />
                        <Form.HelpText>{errors.emg_relation_name && errors.emg_relation_name}</Form.HelpText>
                    </Form.Group>
                </Col>
                <Col xs={12}>
                    <Form.Group controlId="emg_phone">
                        <Form.ControlLabel>Phone number <RS /></Form.ControlLabel>
                        <Form.Control name="emg_phone" onChange={(v: string) => setFieldValue("emg_phone", v)} />
                        <Form.HelpText>{errors.emg_phone && errors.emg_phone}</Form.HelpText>
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group>
                <ButtonGroup style={{ marginTop: 12 }} justified>
                    <Button appearance="primary" type="submit" onClick={(e) => handleSubmit()}>Submit</Button>
                    <Button appearance="ghost" onClick={(e) => window.location.href = '/'}>Cancel</Button>
                </ButtonGroup>
            </Form.Group>
        </Form>
    )
}