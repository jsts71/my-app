import { SignInForm } from "features/user-auth/sign-in.form" 
import withAuthLayout from "layouts/withAuth.layout.hoc"
import { FC } from "react"

export interface SignInViewProps {

}

const SignInView: FC<SignInViewProps> = (props: SignInViewProps) => {
    return (
        <>
            <SignInForm/>
        </>
    )
}


export default withAuthLayout(SignInView)