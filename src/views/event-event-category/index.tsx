import HeaderWithComponent from "features/heading/header-with-components";
import { EventCategoryForm } from "features/event-category/event-category.form";
import { EventCategoryList } from "features/event-category/event-category.list";
import { EventCategoryView } from "features/event-category/event-category.view";
import { FC, useState } from "react";

import "./style.scss"

export interface EventCategoryViewProps {

}

export const EventCategoriesView: FC<EventCategoryViewProps> = (props: EventCategoryViewProps) => {

    const [view, setView] = useState<'new' | 'list' | 'view' | 'update'>('list')
    const [id, setId] = useState<number>(0)

    return (
        <>
            <HeaderWithComponent h='Event category' buttons={[
                { text: "Create New", hidden: view == 'new', color: "green", appearance: "subtle", onClick: () => setView('new') },
                { text: "View List", hidden: view == 'list', color: "blue", appearance: "subtle", onClick: () => setView('list') }
            ]} />

            {view == 'list' &&
                <div className="black-shadow-box-card">
                    <EventCategoryList setView={setView} setId={setId} />
                </div>
            }

            {view == 'new' &&
                <div className="black-shadow-box-card">
                    <EventCategoryForm setView={setView}/>
                </div>
            }

            {view == 'view' && id &&
                <div className="black-shadow-box-card">
                    <EventCategoryView id={id} setView={setView} setId={setId} />
                </div>
            }

            {view == 'update' &&
                <div className="black-shadow-box-card">
                    <EventCategoryForm id={id} setView={setView} />
                </div>
            }

        </>
    )
}