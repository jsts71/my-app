import HeaderWithComponent from "features/heading/header-with-components";
import { TicketForm } from "features/ticket/ticket.form";
import { TicketList } from "features/ticket/ticket.list";
import { TicketViews } from "features/ticket/ticket.view";
import { FC, useState } from "react";
import "./style.scss"

export interface TicketViewProps {

}

export const TicketView: FC<TicketViewProps> = (props: TicketViewProps) => {

    const [view, setView] = useState<'new' | 'list' | 'view' | 'update'>('list')
    const [id, setId] = useState<number>(0)

    return (
        <>
            <HeaderWithComponent h='Ticket type' buttons={[
                { text: "Create New", hidden: view == 'new', color: "green", appearance: "subtle", onClick: () => setView('new') },
                { text: "View List", hidden: view == 'list', color: "blue", appearance: "subtle", onClick: () => setView('list') }
            ]} />

            {view == 'list' &&
                <div className="black-shadow-box-card">
                    <TicketList setView={setView} setId={setId} />
                </div>
            }

            {view == 'new' &&
                <div className="black-shadow-box-card">
                    <TicketForm setView={setView}/>
                </div>
            }

            {view == 'view' && id &&
                <div className="black-shadow-box-card">
                    <TicketViews id={id} setView={setView} setId={setId} />
                </div>
            }

            {view == 'update' &&
                <div className="black-shadow-box-card">
                    <TicketForm id={id} setView={setView} />
                </div>
            }

        </>
    )
}