import HeaderWithComponent from "features/heading/header-with-components";
import { VolunteerApplicationForm } from "features/volunteer/volunteer-application.form";
import { FC } from "react";

export interface VolunteerApplicationViewProps {

}

export const VolunteerApplicationView: FC<VolunteerApplicationViewProps> = (props: VolunteerApplicationViewProps) => {

    return (
        <>
            <HeaderWithComponent h='Application for volunteer position' />
            <div style={{ marginBottom: '50px' }}>
                <VolunteerApplicationForm />
            </div>
        </>
    )
}