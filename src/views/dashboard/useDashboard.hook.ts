
import { useState } from 'react';
import { DefaultButtonProps } from 'components/button/defaultButton';
import SearchIcon from '@rsuite/icons/Search';
import { NavItemProps } from 'rsuite';
import { EventDashboardTab } from 'models/event';

export interface UseDashboardProps {

}

export interface UseDashboardInterface {
    nav_items: NavItemProps[]
    active: EventDashboardTab
    setActive: (value: EventDashboardTab) => void
}

export const useDashboard = (props: UseDashboardProps): UseDashboardInterface => {

    const [active, setActive] = useState<EventDashboardTab>('All')

    const nav_items: NavItemProps[] = [
        {
            children: 'All',
            onSelect: (eventKey?: string, e?: any) => { setActive('All') }
        },
        {
            children: 'Draft',
            onSelect: (eventKey?: string, e?: any) => { setActive('Draft') }
        },
        {
            children: 'Live',
            onSelect: (eventKey?: string, e?: any) => { setActive('Live') }
        },
        {
            children: 'Completed',
            onSelect: (eventKey?: string, e?: any) => { setActive('Completed') }
        },
        {
            children: 'Canceled',
            onSelect: (eventKey?: string, e?: any) => { setActive('Canceled') }
        },
        {
            children: 'Archived',
            onSelect: (eventKey?: string, e?: any) => { setActive('Archived') }
        }
    ]

    return {
        nav_items,
        active,
        setActive
    }

}