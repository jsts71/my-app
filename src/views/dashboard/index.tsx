import { FC, useState } from "react"
import { useDashboard } from "./useDashboard.hook"
import HeaderWithComponent from "features/heading/header-with-components"
import { DefaultButtonProps } from "components/button/defaultButton"
import FacebookOfficialIcon from '@rsuite/icons/legacy/FacebookOfficial';
import { DefaultHeader } from "features/headers/default.headers";
import { EventDashboard } from "features/event-dashboard";

export interface DashboardViewProps {

}

export const DashboardView: FC<DashboardViewProps> = (props: DashboardViewProps) => {

    const { nav_items, active, setActive } = useDashboard({})

    let defaultButtonList: DefaultButtonProps[] = [
        { text: 'Button', appearance: 'primary', icon: <FacebookOfficialIcon />, onClick: () => { console.log('btn 1 click!') } },
        { text: 'Button', onClick: () => { console.log('btn 2 click!') } }
    ]
    const [buttons, setButtons] = useState<DefaultButtonProps[]>(defaultButtonList)

    const switchView = () => {
        if (active == 'All') return <>All</>
        if (active == 'Draft') return <>Draft</>
        if (active == 'Live') return <>Live</>
        if (active == 'Completed') return <>Completed</>
        if (active == 'Canceled') return <>Canceled</>
        if (active == 'Archived') return <>Archived</>
    }

    return (
        <>
            <HeaderWithComponent h='Dashboard' buttons={buttons} />
            <DefaultHeader appearance="subtle" justified nav_items={nav_items} active={active} />

            <div className="create-event-form">
                <EventDashboard active={active}/>
            </div>
        </>
    )
}