import { FC, useState } from "react"
import { useCreateEvent } from "./useCreateEvent.hook"
import HeaderWithComponent from "features/heading/header-with-components"
import { DefaultButtonProps } from "components/button/defaultButton"
import FacebookOfficialIcon from '@rsuite/icons/legacy/FacebookOfficial';
import { DefaultHeader } from "features/headers/default.headers";
import { WhatEventForm } from "features/event-create/what-event.form";
import { WhenEventForm } from "features/event-create/when-event.form";
import { HowMuchEventForm } from "features/event-create/how-much-event.form";
import { StartSellingEventForm } from "features/event-create/start-selling-event.form";
import { WhereEventForm } from "features/event-create/where-event.form";
import "./style.scss"

export interface CreateEventViewProps {

}

export const CreateEventView: FC<CreateEventViewProps> = (props: CreateEventViewProps) => {

    const { nav_items, active, setActive, id, setId } = useCreateEvent({})

    let defaultButtonList: DefaultButtonProps[] = [
        { text: 'Button', appearance: 'primary', icon: <FacebookOfficialIcon />, onClick: () => { console.log('btn 1 click!') } },
        { text: 'Button', onClick: () => { console.log('btn 2 click!') } }
    ]
    const [buttons, setButtons] = useState<DefaultButtonProps[]>(defaultButtonList)

    const switchView = () => {
        if (active == 'What') return <WhatEventForm setActive={setActive} id={id} setId={setId} />
        if (active == 'Where') return <WhereEventForm setActive={setActive} id={id} />
        if (active == 'When') return <WhenEventForm setActive={setActive} id={id} />
        if (active == 'How much') return <HowMuchEventForm setActive={setActive} id={id} />
        if (active == 'Start selling') return <StartSellingEventForm setActive={setActive} id={id} />
    }

    return (
        <>
            <HeaderWithComponent h={id ? 'Update event' : 'Create event'} buttons={buttons} />
            <DefaultHeader appearance="subtle" justified nav_items={nav_items} active={active} />
            <div className="create-event-form">
                {switchView()}
            </div>
        </>
    )
}