import { TicketTab } from 'models/event';
import { useState } from 'react';
import { NavItemProps } from 'rsuite';

export interface UseCreateEventProps {

}

export interface UseCreateEventInterface {
    nav_items: NavItemProps[]
    active: TicketTab
    setActive: (value: TicketTab) => void
    id: number
    setId: (value: number) => void
}

export const useCreateEvent = (props: UseCreateEventProps): UseCreateEventInterface => {

    const [id, setId] = useState(parseInt(window.location.search.substring(1, window.location.search.length)))
    const [active, setActive] = useState<TicketTab>('What')
    const nav_items: NavItemProps[] = [
        {
            children: 'What',
            onSelect: (eventKey?: string, e?: any) => { setActive('What'); console.log('1') }
        },
        {
            children: 'Where',
            onSelect: (eventKey?: string, e?: any) => { setActive('Where'); console.log('2') }
        },
        {
            children: 'When',
            onSelect: (eventKey?: string, e?: any) => { setActive('When'); console.log('3') }
        },
        {
            children: 'How much',
            onSelect: (eventKey?: string, e?: any) => { setActive('How much'); console.log('4') }
        },
        {
            children: 'Start selling',
            onSelect: (eventKey?: string, e?: any) => { setActive('Start selling'); console.log('5') }
        },
    ]

    return {
        nav_items,
        active,
        setActive,
        id, 
        setId
    }

}