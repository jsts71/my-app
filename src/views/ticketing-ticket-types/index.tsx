import HeaderWithComponent from "features/heading/header-with-components";
import { TicketTypesForm } from "features/ticket-types/ticket-types.form";
import { TicketTypesList } from "features/ticket-types/ticket-types.list";
import { TicketTypeView } from "features/ticket-types/ticket-types.view";
import { FC, useState } from "react";

import "./style.scss"

export interface TicketTypesViewProps {

}

export const TicketTypesView: FC<TicketTypesViewProps> = (props: TicketTypesViewProps) => {

    const [view, setView] = useState<'new' | 'list' | 'view' | 'update'>('list')
    const [id, setId] = useState<number>(0)

    return (
        <>
            <HeaderWithComponent h='Ticket type' buttons={[
                { text: "Create New", hidden: view == 'new', color: "green", appearance: "subtle", onClick: () => setView('new') },
                { text: "View List", hidden: view == 'list', color: "blue", appearance: "subtle", onClick: () => setView('list') }
            ]} />

            {view == 'list' &&
                <div className="black-shadow-box-card">
                    <TicketTypesList setView={setView} setId={setId} />
                </div>
            }

            {view == 'new' &&
                <div className="black-shadow-box-card">
                    <TicketTypesForm setView={setView}/>
                </div>
            }

            {view == 'view' && id &&
                <div className="black-shadow-box-card">
                    <TicketTypeView id={id} setView={setView} setId={setId} />
                </div>
            }

            {view == 'update' &&
                <div className="black-shadow-box-card">
                    <TicketTypesForm id={id} setView={setView} />
                </div>
            }

        </>
    )
}