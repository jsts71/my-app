
import { PurchaseTicketTab } from 'models/ticket';
import { useState } from 'react';
import { NavItemProps } from 'rsuite';

export interface UsePurchaseVolunteerTicketProps {
}

export interface UsePurchaseVolunteerTicketInterface {

    nav_items: NavItemProps[]
    active: PurchaseTicketTab
    setActive: (value: PurchaseTicketTab) => void
}

export const usePurchaseVolunteerTicket = (props: UsePurchaseVolunteerTicketProps): UsePurchaseVolunteerTicketInterface => {


    const [active, setActive] = useState<PurchaseTicketTab>('Introduction')
    const nav_items: NavItemProps[] = [
        {
            children: 'Introduction',
            onSelect: (eventKey?: string, e?: any) => { setActive('Introduction'); console.log('1') }
        },
        {
            children: 'Aggreement',
            onSelect: (eventKey?: string, e?: any) => { setActive('Aggreement'); console.log('2') }
        },
        {
            children: 'Whs',
            onSelect: (eventKey?: string, e?: any) => { setActive('Whs'); console.log('3') }
        },
        {
            children: 'Template',
            onSelect: (eventKey?: string, e?: any) => { setActive('Template'); console.log('4') }
        },
        {
            children: 'Payment',
            onSelect: (eventKey?: string, e?: any) => { setActive('Payment'); console.log('5') }
        },
        {
            children: 'Ticket',
            onSelect: (eventKey?: string, e?: any) => { setActive('Ticket'); console.log('6') }
        },
    ]

    return {
        nav_items,
        active,
        setActive
    }

}