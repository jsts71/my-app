import { FC, useEffect } from "react"
import { usePurchaseVolunteerTicket } from "./usePurchaseVolunteerTicket.hook";
import HeaderWithComponent from "features/heading/header-with-components"
import "./style.scss"
import { Template } from "features/purchase-ticket/template";
import { Payment } from "features/purchase-ticket/payments";
import { Ticket } from "features/purchase-ticket/ticket";
import { Introduction } from "features/purchase-ticket/introduction";
import { Aggreement } from "features/purchase-ticket/aggreement";
import { Whs } from "features/purchase-ticket/whs";
import { DefaultHeader } from "features/headers/default.headers";
import { useGetVolunteerTicketByIdMutation } from "features/volunteer-ticket/redux/apis";
import { useParams } from "react-router-dom";

export interface PurchaseVolunteerTicketViewProps {

}

export const PurchaseVolunteerTicketView: FC<PurchaseVolunteerTicketViewProps> = (props: PurchaseVolunteerTicketViewProps) => {

    const { id: ticketId, eventId } = useParams()

    const { nav_items, active, setActive } = usePurchaseVolunteerTicket({})

    const [getVolunteerTicketById, { data, isLoading, isSuccess }] = useGetVolunteerTicketByIdMutation()

    useEffect(() => {
        ticketId && getVolunteerTicketById(ticketId)
    }, [ticketId])


    const switchView = () => {
        if (active == 'Introduction') return isSuccess && <Introduction setActive={setActive} data={data} />
        if (active == 'Aggreement') return isSuccess && <Aggreement setActive={setActive} data={data} />
        if (active == 'Whs') return isSuccess && <Whs setActive={setActive} data={data} />
        if (active == 'Template') return isSuccess && <Template setActive={setActive} data={data} />
        if (active == 'Payment') return isSuccess && <Payment setActive={setActive} data={data} />
        if (active == 'Ticket') return isSuccess && <Ticket setActive={setActive} data={data} />
    }

    return (
        <>
            <HeaderWithComponent h='Purchase volunteer ticket' />
            {/* buttons={buttons} /> */}
            <DefaultHeader appearance="subtle" justified nav_items={nav_items} active={active} />
            <div className="black-shadow-box-card">
                {switchView()}
            </div>
        </>
    )
}