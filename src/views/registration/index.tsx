import { RegistrationForm } from "features/user-auth/registration.form"
import withAuthLayout from "layouts/withAuth.layout.hoc"
import { FC } from "react"

export interface RegistrationViewProps {

}

const RegistrationView: FC<RegistrationViewProps> = (props: RegistrationViewProps) => {
    return (
        <>
            <RegistrationForm/>
        </>
    )
}


export default withAuthLayout(RegistrationView)