import HeaderWithComponent from "features/heading/header-with-components";
import { VolunteerTicketForm } from "features/volunteer-ticket/volunteer-ticket.form";
import { VolunteerTicketList } from "features/volunteer-ticket/volunteer-ticket.list";
import { VolunteerTicketsView } from "features/volunteer-ticket/volunteer-ticket.view";
import { FC, useState } from "react";

import "./style.scss"

export interface VolunteerTicketViewProps {

}

export const VolunteerTicketView: FC<VolunteerTicketViewProps> = (props: VolunteerTicketViewProps) => {

    const [view, setView] = useState<'new' | 'list' | 'view' | 'update'>('list')
    const [id, setId] = useState<number>(0)

    return (

        <>
            <HeaderWithComponent h='Volunteer ticket' buttons={[
                { text: "Create New", hidden: view == 'new', color: "green", appearance: "subtle", onClick: () => setView('new') },
                { text: "View List", hidden: view == 'list', color: "blue", appearance: "subtle", onClick: () => setView('list') }
            ]} />

            {view == 'list' &&
                <div className="black-shadow-box-card">
                    <VolunteerTicketList setView={setView} setId={setId} />
                </div>
            }

            {view == 'new' &&
                <div className="black-shadow-box-card">
                    <VolunteerTicketForm  setView={setView} />
                </div>
            }

            {view == 'view' && id &&
                <div className="black-shadow-box-card">
                    <VolunteerTicketsView id={id} setView={setView} setId={setId} />
                </div>
            }

            {view == 'update' &&
                <div className="black-shadow-box-card">
                    <VolunteerTicketForm id={id} setView={setView} />
                </div>
            }

        </>
    )
}