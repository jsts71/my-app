import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';
export type TicketTab = 'What' | 'Where' | 'When' | 'How much' | 'Start selling'
export type EventDashboardTab = 'All' | 'Draft' | 'Live' | 'Completed' | 'Canceled' | 'Archived'

export interface EventCategory {
    id?: number,
    name?: string,
    description?: string
}

export const EventCategoryInitialValues: EventCategory = {
    id: undefined,
    name: '',
    description: ''
}

export const EventCategoryValidationSchema: ObjectSchema<EventCategory> = object({
    id: number(),
    name: string().required().max(100),
    description: string().required().max(2000)
});

export interface WhatEvent {
    id?: number,
    title: string,
    category: number,
    description: string,
    status: 'Draft' | 'Published' | 'Canceled' | string,
    published: boolean,
    // type: number,
    // stream?: number,
    // stream_link?: string,
}

export const WhatEventInitialValues: WhatEvent = {
    id: undefined,
    title: '',
    description: '',
    status: 'Draft',
    category: 0,
    published: false,
}

export interface WhereEvent {
    id?: number,
    name?: string,
    description?: string,
    event?: number,
}

export const WhereEventInitialValues: WhereEvent = {
    id: undefined,
    name: '',
    description: '',
    event: 0,
}

export interface WhenEvent {
    id?: number,
    // timezone?: number,
    start_date?: Date,
    start_time?: string,
    end_date?: Date,
    end_time?: string,
    event?: number,
}

export const WhenEventInitialValues: WhenEvent = {
    id: undefined,
    // timezone?: number,
    start_date: undefined,
    start_time: undefined,
    end_date: undefined,
    end_time: undefined,
    event: undefined,
}

export interface HowMuchEvent {
    id?: number,
    quantity?: number,
    price?: number,
    event?: number,
}

export const HowMuchEventInitialValues: HowMuchEvent = {
    id: undefined,
    quantity: 1,
    price: 1,
    event: 0,
}

export interface StartSellingEvent {
    id?: number,
    refund: boolean,
    policy?: string,
    terms_n_conditions?: string,
    stripe_payment?: boolean,
    event: number,
}

export const StartSellingEventInitialValues: StartSellingEvent = {
    id: undefined,
    refund: false,
    policy: '',
    terms_n_conditions: '',
    stripe_payment: true,
    event: 0,
}