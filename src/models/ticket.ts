import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';

export type PurchaseTicketTab = 'Introduction' | 'Aggreement' | 'Whs' | 'Template' | 'Payment' | 'Ticket'

export class TicketFilter {
    private page?: number;
    private is_volunteer_type?: 'True' | 'False';

    constructor(page?: number, is_volunteer_type?: 'True' | 'False') {
        this.page = page;
        this.is_volunteer_type = is_volunteer_type;
    }

    public setPage(page: number) {
        this.page = page
    }

    public setIsVoulteerType(is_volunteer_type: 'True' | 'False') {
        this.is_volunteer_type = is_volunteer_type
    }

    public to_string() {
        var filter = ''
        if (this.page) filter = filter + '&page=' + this.page
        if (this.is_volunteer_type) filter = filter + '&is_volunteer_type=' + this.is_volunteer_type
        return filter.substring(1, filter.length)
    }
}

export interface TicketType {
    id?: number;
    name: string;
    is_volunteer_type: boolean;
    discount_percent: number;
    description: string;
}

export const ticketTypeInitialValues: TicketType = {
    id: undefined,
    name: '',
    is_volunteer_type: false,
    discount_percent: 0,
    description: ''
}

export const ticketTypeValidationSchema: ObjectSchema<TicketType> = object({
    id: number(),
    name: string().required().max(100),
    is_volunteer_type: boolean().required(),
    discount_percent: number().required().max(100).min(0),
    description: string().required().max(3000)
});

// Volunteer ticket #########################################################

export class VolunteerTicketFilter {
    private page?: number;
    private is_volunteer_type?: 'True' | 'False';

    constructor(page?: number, is_volunteer_type?: 'True' | 'False') {
        this.page = page;
        this.is_volunteer_type = is_volunteer_type;
    }

    public setPage(page: number) {
        this.page = page
    }

    public setIsVoulteerType(is_volunteer_type: 'True' | 'False') {
        this.is_volunteer_type = is_volunteer_type
    }

    public to_string() {
        var filter = ''
        if (this.page) filter = filter + '&page=' + this.page
        if (this.is_volunteer_type) filter = filter + '&is_volunteer_type=' + this.is_volunteer_type
        return filter.substring(1, filter.length)
    }
}

export interface VolunteerTicket {
    id?: number;
    name: string;
    event?: number;
    ticket_type?: number;
    agreement_template: string;
    introduction_template: string;
    whs_template: string;
    link: string;
    ticket_template: string;
    terms_n_condition: string;
    note: string;
    public_key: string;
    secret_key: string;
}

export const VolunteerTicketInitialValues: VolunteerTicket = {
    id: undefined,
    name: '',
    event: undefined,
    ticket_type: undefined,
    agreement_template: '',
    introduction_template: '',
    whs_template: '',
    link: 'no value',
    ticket_template: '',
    terms_n_condition: '',
    note: '',
    public_key: '',
    secret_key: ''
}

export const VolunteerTicketValidationSchema: ObjectSchema<VolunteerTicket> = object({
    id: number(),
    name: string().required().max(100),
    event: number().required(),
    ticket_type: number().required(),
    agreement_template: string().required(),
    introduction_template: string().required(),
    whs_template: string().required(),
    link: string().required(),
    ticket_template: string().required(),
    terms_n_condition: string().required(),
    note: string().required(),
    public_key: string().required(),
    secret_key: string().required(),
});


export interface Ticket {
    id?: number;
    name: string;
    is_volunteer_type: boolean;
    discount_percent: number;
    description: string;
}

export const ticketInitialValues: Ticket = {
    id: undefined,
    name: '',
    is_volunteer_type: false,
    discount_percent: 0,
    description: ''
}

export const ticketValidationSchema: ObjectSchema<Ticket> = object({
    id: number(),
    name: string().required().max(100),
    is_volunteer_type: boolean().required(),
    discount_percent: number().required().max(100).min(0),
    description: string().required().max(3000)
});