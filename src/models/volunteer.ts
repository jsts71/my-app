import { object, string, number, date, InferType, ObjectSchema, ref } from 'yup';

export interface Volunteer extends VolunteerApplication {
    id?: number;
}

export interface VolunteerApplication {
    first_name: string;
    last_name: string;
    prefered_name?: string;
    email: string;
    confirm_email: string;
    phone: string;
    zip?: string;
    city: string;
    state: string;
    country: string;
    fb_link: string;
    dob?: Date;
    experience: string;
    experience_description?: string;
    current_job: string;
    qualifications: string[];
    about?: string;
    comments?: string;
    preferred_tasks: string[];
    preferred_tasks_info?: string;
    preferred_time_frame: string[];
    volunteering_with_friends?: string[];
    volunteering_with_head?: boolean;
    dept_head_name?: string;
    emg_first_name: string;
    emg_last_name?: string;
    emg_relation_name: string;
    emg_phone: string;
}


// export interface VolunteerTicket {
//     id?: number;
//     ticket_type?: number;
//     ticket_cost?: number;
//     aggrement_template?: number;
//     introduction_template?: number;
//     whs_template?: number;
//     link?: string;
//     payment_method?: number;
//     ticket_template?: number;
// }

