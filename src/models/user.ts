export interface User extends UserRegistration {
    id?: number;
    active?: boolean;
}

export interface UserRegistration {

    first_name: string;
    last_name?: string;
    username: string;
    email: string;
    password: string;
    confirm_password: string;
}