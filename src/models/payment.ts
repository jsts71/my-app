import { object, string, number, array, boolean, InferType, ObjectSchema, ref, date } from 'yup';

export interface StripePayment {

    id?: number,
    email: string,
    currency: string,
    volunteer_ticket: number,
    purchased: boolean
}

export const StripePaymentInitialValues: StripePayment = {
    id: undefined,
    email: '',
    currency: 'aud',
    volunteer_ticket: 0,
    purchased: false
}

export const StripePaymentValidationSchema: ObjectSchema<StripePayment> = object({
    id: number(),
    email: string().required().max(100),
    currency: string().required().max(100),
    volunteer_ticket: number().required(),
    purchased: boolean().required()
});
