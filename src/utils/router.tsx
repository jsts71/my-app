import { createBrowserRouter } from "react-router-dom";

import { CurrentEventPage } from "pages/side-nav-event-current-event";
import { DashboardPage } from "pages/top-nav-dashboard";
import { ApplicationBuilderPage } from "pages/side-nav-application-application-builder";
import { ApplicationEmailsPage } from "pages/side-nav-application-application-emails";
import { MarketApplicationPage } from "pages/side-nav-application-market-application";
import { PerformenceApplicationPage } from "pages/side-nav-application-performers-application";
import { ApplicationVolunteerPage } from "pages/side-nav-application-volunteer-application";
import { EmailTemplateEditorPage } from "pages/side-nav-email-templates-editor";
import { EventDetailsPage } from "pages/side-nav-event-settings-details";
import { EventWhereWhenPage } from "pages/side-nav-event-settings-where-n-when";
import { PortalUserPage } from "pages/side-nav-portal-user";
import { SalesReportPage } from "pages/side-nav-sales-n-report";
import { StaffPage } from "pages/side-nav-staff";
import { DiscountCodePage } from 'pages/side-nav-ticketing-discount-codes/index';
import { TicketingIntroductionPage } from "pages/side-nav-ticketing-introduction";
import { TicketingPaymentPage } from "pages/side-nav-ticketing-payment";
import { TicketingStaffCrewPage } from "pages/side-nav-ticketing-staff-crew-ticketing";
import { TicketingTermsConditionPage } from 'pages/side-nav-ticketing-terms-n-condition/index';
import { TicketTypesPage } from 'pages/side-nav-ticketing-ticket-types/index';
import { VolunteerBulkDeletePage } from "pages/side-nav-volunteer-bulk-delete";
import { VolunteerEmailPage } from "pages/side-nav-volunteer-email";
import { VolunteerExportPage } from "pages/side-nav-volunteer-export";
import { VolunteerFinancePage } from "pages/side-nav-volunteer-finance";
import { VolunteerRegisterPage } from "pages/side-nav-volunteer-register-volunteer";
import { VolunteerShiftTimePage } from "pages/side-nav-volunteer-shift-time";
import { VolunteerApplicationPage } from "pages/side-nav-volunteer-volunteer-application";
import { VolunteerPositionPage } from "pages/side-nav-volunteer-volunteer-position";
import { VolunteerTicketPage } from "pages/side-nav-volunteer-volunteer-ticket";
import { ProfilePage } from "pages/top-nav-profile";
import { RegistrationPage } from "pages/top-nav-registration";
import { SignInPage } from "pages/top-nav-signin";
import { TemplateListPage } from "pages/side-nav-email-templates-list-of-template";
import { CreateEventPage } from "pages/top-nav-create-event";
import { EventCategoryPage } from "pages/side-nav-event-category";
import { PurchaseVolunteerTicketPage } from "pages/purchase-volunteer-ticket";
import { TicketPage } from "pages/side-nav-ticketing-ticket";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <DashboardPage />,
    },
    {
        path: "/application-builder",
        element: <ApplicationBuilderPage />,
    },
    {
        path: "/application-email",
        element: <ApplicationEmailsPage />,
    },
    {
        path: "/market-application",
        element: <MarketApplicationPage />,
    },
    {
        path: "/application-performance",
        element: <PerformenceApplicationPage />,
    },
    {
        path: "/volunteers-application",
        element: <ApplicationVolunteerPage />,
    },
    {
        path: "/template-editor",
        element: <EmailTemplateEditorPage />,
    },
    {
        path: "/template-list",
        element: <TemplateListPage />,
    },
    {
        path: "/current-event",
        element: <CurrentEventPage />,
    },
    {
        path: "/event-details",
        element: <EventDetailsPage />,
    },
    {
        path: "/event-where-n-when",
        element: <EventWhereWhenPage />,
    },
    {
        path: "/event-category",
        element: <EventCategoryPage />,
    },
    {
        path: "/user-portal",
        element: <PortalUserPage />,
    },
    {
        path: "/sales-report",
        element: <SalesReportPage />,
    },
    {
        path: "/staff",
        element: <StaffPage />,
    },
    {
        path: "/discount-code",
        element: <DiscountCodePage />,
    },
    {
        path: "/ticketing-introduction",
        element: <TicketingIntroductionPage />,
    },
    {
        path: "/ticketing-payment",
        element: <TicketingPaymentPage />,
    },
    {
        path: "/ticketing-staff-crew",
        element: <TicketingStaffCrewPage />,
    },
    {
        path: "/terms-n-condition",
        element: <TicketingTermsConditionPage />,
    },
    {
        path: "/ticket",
        element: <TicketPage />,
    },
    {
        path: "/ticket-types",
        element: <TicketTypesPage />,
    },
    {
        path: "/volunteer-bulk-delete",
        element: <VolunteerBulkDeletePage />,
    },
    {
        path: "/volunteer-email",
        element: <VolunteerEmailPage />,
    },
    {
        path: "/volunteer-export",
        element: <VolunteerExportPage />,
    },
    {
        path: "/volunteer-finance",
        element: <VolunteerFinancePage />,
    },
    {
        path: "/volunteer-register",
        element: <VolunteerRegisterPage />,
    },
    {
        path: "/volunteer-shift-time",
        element: <VolunteerShiftTimePage />,
    },
    {
        path: "/volunteer-application",
        element: <VolunteerApplicationPage />,
    },
    {
        path: "/volunteer-position",
        element: <VolunteerPositionPage />,
    },
    {
        path: "/volunteer-ticket",
        element: <VolunteerTicketPage />,
    },
    {
        path: "/create-event",
        element: <CreateEventPage />,
    },
    {
        path: "/profile",
        element: <ProfilePage />,
    },
    {
        path: "/registration",
        element: <RegistrationPage />,
    },
    {
        path: "/sign-in",
        element: <SignInPage />,
    },
    {
        path: "/purchase-volunteer-ticket/:id/:eventId",
        element: <PurchaseVolunteerTicketPage />,
    },
]);