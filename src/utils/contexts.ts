import { createContext } from 'react';

export const SuperContext = createContext({
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight
});