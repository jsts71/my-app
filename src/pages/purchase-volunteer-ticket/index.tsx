import { FC } from "react"
import { PurchaseVolunteerTicketView } from "views/purchase-volunteer-ticket"

export interface PurchaseVolunteerTicketPageProps {

}

export const PurchaseVolunteerTicketPage: FC<PurchaseVolunteerTicketPageProps> = (props: PurchaseVolunteerTicketPageProps) => {
    return (
        <>
            <PurchaseVolunteerTicketView/>
        </>
    )
}