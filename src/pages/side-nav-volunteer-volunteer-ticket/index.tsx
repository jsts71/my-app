import { FC } from "react"
import { VolunteerTicketView } from "views/volunteer-volunteer-ticket"

export interface VolunteerTicketPageProps {

}

export const VolunteerTicketPage: FC<VolunteerTicketPageProps> = (props: VolunteerTicketPageProps) => {
    return (
        <>
            <VolunteerTicketView/>
        </>
    )
}