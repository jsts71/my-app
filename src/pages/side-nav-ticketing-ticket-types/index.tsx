import { FC } from "react"
import { TicketTypesView } from "views/ticketing-ticket-types"

export interface TicketTypesPageProps {

}

export const TicketTypesPage: FC<TicketTypesPageProps> = (props: TicketTypesPageProps) => {
    return (
        <>
            <TicketTypesView/>
        </>
    )
}