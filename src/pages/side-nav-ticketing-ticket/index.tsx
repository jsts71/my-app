import { FC } from "react"
import { TicketView } from "views/ticketing-ticket"

export interface TicketPageProps {

}

export const TicketPage: FC<TicketPageProps> = (props: TicketPageProps) => {
    return (
        <>
            <TicketView/>
        </>
    )
}