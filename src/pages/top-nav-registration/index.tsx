import { FC } from "react"
import RegistrationView from "views/registration"

export interface RegistrationPageProps {

}

export const RegistrationPage: FC<RegistrationPageProps> = (props: RegistrationPageProps) => {
    return (
        <>
            <RegistrationView/>
        </>
    )
}