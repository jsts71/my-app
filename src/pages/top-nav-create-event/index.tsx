import { FC } from "react"
import { CreateEventView } from "views/create-event"

export interface CreateEventPageProps {

}

export const CreateEventPage: FC<CreateEventPageProps> = (props: CreateEventPageProps) => {
    return (
        <>
            <CreateEventView/>
        </>
    )
}