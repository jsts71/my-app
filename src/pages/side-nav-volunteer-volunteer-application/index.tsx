import { FC } from "react"
import { VolunteerApplicationView } from "views/volunteer-volunteer-application"

export interface VolunteerApplicationPageProps {

}

export const VolunteerApplicationPage: FC<VolunteerApplicationPageProps> = (props: VolunteerApplicationPageProps) => {
    return (
        <>
            <VolunteerApplicationView />
        </>
    )
}