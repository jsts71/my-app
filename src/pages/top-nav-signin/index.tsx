import { FC } from "react"
import SignInView from 'views/sign-in/index';

export interface SignInPageProps {

}

export const SignInPage: FC<SignInPageProps> = (props: SignInPageProps) => {
    return (
        <>
            <SignInView />
        </>
    )
}