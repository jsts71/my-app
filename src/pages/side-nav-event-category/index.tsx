import { FC } from "react"
import { EventCategoriesView } from "views/event-event-category"

export interface EventCategoryPageProps {

}

export const EventCategoryPage: FC<EventCategoryPageProps> = (props: EventCategoryPageProps) => {
    return (
        <>
            <EventCategoriesView/>
        </>
    )
}