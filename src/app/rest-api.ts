import { createApi } from '@reduxjs/toolkit/query/react'
import type { BaseQueryFn } from '@reduxjs/toolkit/query/react'
import axios from 'axios'
import type { AxiosRequestConfig, AxiosError } from 'axios'

let BACKEND_URL = process.env.REACT_APP_BACKEND_URL ?? ''

const axiosBaseQuery =
  (
    { baseUrl }: { baseUrl: string } = { baseUrl: '' }
  ): BaseQueryFn<
    {
      url: string
      method: AxiosRequestConfig['method']
      data?: AxiosRequestConfig['data']
      params?: AxiosRequestConfig['params']
    },
    unknown,
    unknown
  > =>
    async ({ url, method, data, params }) => {
      console.log(BACKEND_URL, baseUrl + url)
      try {
        const result = await axios({ url: baseUrl + url, method, data, params })

        return { data: result.data }
      } catch (axiosError) {
        let err = axiosError as AxiosError

        if (err && err.response?.status === 401) {
          // const refreshResult = await axios('/refreshToken', api, extraOptions)

          // if (refreshResult.data) {
          // store the new token
          // api.dispatch(tokenReceived(refreshResult.data))
          // retry the initial query
          // result = await baseQuery(args, api, extraOptions)

          // return { data: result.data }

        } else {
          // api.dispatch(loggedOut())
        }


        return {
          error: {
            url: baseUrl + url,
            status: err.response?.status,
            data: err.response?.data || err.message,
          },
        }
      }
    }

const rest_api = createApi({
  baseQuery: axiosBaseQuery({
    baseUrl: BACKEND_URL,
  }),
  endpoints: (build) => ({}),
})

export default rest_api